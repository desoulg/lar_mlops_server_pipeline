## Overview

This is a template for kedro project using the Server MLOps Architecture.

Take a look at the [MLOps Architecture documentation](https://whirlpool-lar.atlassian.net/wiki/spaces/ITDS/pages/2064842766/Playbook+Intensive+MLOps+Architecture) to know the MLOps steps.


## Creating a project
First to create a project from this template, it is necessary to download the `cookiecutter` package from `pip` and then start the template from this repository.

```
pip install cookiecutter
cookiecutter https://DESOULG@bitbucket.org/desoulg/lar_mlops_server_pipeline.git
```

After that, the template will require some variables to create the code.
* Project Name: The name of the project, there is no validation for this field.
* Pipeline Name: The name of the pipeline (python package), this field respect the following regex: `^[_a-zA-Z][_a-zA-Z0-9]{,18}$` 
* KFP URL: The Host Kubeflow URL that is used by Python KFP SDK.
* Metrics Path: The relative path where metrics lives. (Just `.csv` files is supported now)


## Contributing

To contribute to this initiative, please do your modifications and create a pull-request to master branch with at least a ML Engineer as revisor. 