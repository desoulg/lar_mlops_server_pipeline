import re
import sys


PIPELINE_NAME_REG = r'^[_a-zA-Z][_a-zA-Z0-9]{,18}$'
METRICS_PATH_REG = r'.*?\.csv$'

pipeline_name = '{{ cookiecutter.pipeline_name }}'
metrics_path = '{{ cookiecutter.metrics_path }}'

print("\n")

if not re.match(PIPELINE_NAME_REG, pipeline_name):
    print('ERROR: %s is not a valid pipeline name!' % pipeline_name)
    print("Please, follow the regex pipeline name: %s" % PIPELINE_NAME_REG)
    # exits with status 1 to indicate failure
    sys.exit(1)

if not re.match(METRICS_PATH_REG, metrics_path):
    print('ERROR: %s is not a valid metrics path!' % metrics_path)
    print("Please, follow the regex pipeline name: %s" % METRICS_PATH_REG)
    # exits with status 1 to indicate failure
    sys.exit(1)