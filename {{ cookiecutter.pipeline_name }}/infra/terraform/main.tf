terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file("credentials.json")
  project = var.project
  region  = var.region
}

provider "google-beta" {
  credentials = file("credentials.json")
  project = var.project
  region  = var.region
}

#CloudSQL
##QUEUE
resource "google_sql_database_instance" "cloud_sql" {
  name   = ""
  database_version = "MYSQL_5_7"
  region = var.region
  settings {
    tier = "db-f1-micro" #change
  }
}

#Cloud Storage
##StorageName
resource "google_storage_bucket" "storage_name" {
  name          = "storage_name"
  location      = var.region
}
##StorageMetaData
resource "google_storage_bucket" "meta_storage" {
  name          = "meta_storage"
  location      = var.region
}
##StorageModels
resource "google_storage_bucket" "models_storage" {
  name          = "models_storage"
  location      = var.region
}