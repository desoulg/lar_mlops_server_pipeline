import os
import json
import pandas as pd
import yaml
from collections.abc import MutableMapping

def _flatten_dict(d: MutableMapping, sep: str= '.') -> MutableMapping:
    [flat_dict] = pd.json_normalize(d, sep=sep).to_dict(orient='records')
    return flat_dict

with open("conf/base/config.yml") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

tag = os.getenv("TAG", "")
run_id = os.getenv("RUN_ID", "")

df = pd.read_csv(f"{config.data_bucket}/{tag}/{run_id}/{{cookiecutter.metrics_path}}", index_col=0)
flat_dict = _flatten_dict(df.to_dict())

metrics_json = []

for k, v in flat_dict.items():
    metrics_json.append({"name": k.replace(".", "_"), "numberValue": round(v, 4), "format": "RAW"})

metrics = {'metrics': metrics_json}

with open("mlpipeline-metrics.json", 'w') as f:
    json.dump(metrics, f)