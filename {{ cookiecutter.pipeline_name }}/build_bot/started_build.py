from json import dumps
from httplib2 import Http
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--commit', type=str, help='Commit Hash')
parser.add_argument('--build', type=str, help='Build Id')
parser.add_argument('--url', type=str, help='Chat URL')

args = parser.parse_args()


def main():
    """Hangouts Chat incoming webhook quickstart."""
    
    bot_message = {"cards": [{"sections": [{"widgets": [{"textParagraph": {"text": "<b>Build ({{ cookiecutter.project_name }}) <font color=\"#EDDA74\">started</font></b><br /> from a commit<br /><p><b>Commit Hash:</b> "+ args.commit+"</p><br /><p><b>Build Id:</b> "+args.build+"</p>"}}]}]}]}
    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

    http_obj = Http()

    response = http_obj.request(
        uri=args.url,
        method='POST',
        headers=message_headers,
        body=dumps(bot_message),
    )

    print(response)

if __name__ == '__main__':
    main()