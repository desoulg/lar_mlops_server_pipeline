from json import dumps
from httplib2 import Http
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--url', type=str, help='Chat URL')

args = parser.parse_args()


def main():
    """Hangouts Chat incoming webhook quickstart."""
    
    bot_message = {"cards": [{"sections": [{"widgets": [{"textParagraph": {"text": "<b>Build ({{ cookiecutter.project_name }}) <font color=\"#4CC417\">Finished</font></b><br />"}}]}]}]}
    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

    http_obj = Http()

    response = http_obj.request(
        uri=args.url,
        method='POST',
        headers=message_headers,
        body=dumps(bot_message),
    )

    print(response)

if __name__ == '__main__':
    main()