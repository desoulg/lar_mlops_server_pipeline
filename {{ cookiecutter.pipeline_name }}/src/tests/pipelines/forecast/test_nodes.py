from src.{{cookiecutter.pipeline_name}}.pipelines.forecast.nodes import forecasting, join_results
import pytest


@pytest.fixture()
def final_results(training_data, crafted_estimators, forecast_params):
    forecastings =  forecasting(training_data, best_estimators=crafted_estimators, **forecast_params)
    return join_results(forecastings)

def test_length_of_timeseries(final_results, forecast_params):
    assert len(final_results) == forecast_params["fh"] + 3


def test_length_of_notnull_timeseries(final_results):
    assert len(final_results.dropna()) == 3