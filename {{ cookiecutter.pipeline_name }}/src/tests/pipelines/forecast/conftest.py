import pandas as pd
import numpy as np
import pytest
from datetime import datetime
from ..utils import convert_dtypes


@pytest.fixture(scope="module")
def forecast_params():
    return {
        "start_date": datetime.strptime("2019-01-01", "%Y-%m-%d").date(),
        "fh": 12,
        "imr_factor": 2,
        "mr_range": 6,
        "split_info":{
                        "level": "agrup",
                        "n_splits": 1
                    },
        "split": 0
    }



@pytest.fixture(scope="module")
def training_data():
    df = pd.DataFrame({
        "level": [1,1,1,1,1,1],
        "periodo": ["2020-08-01 00:00:00","2020-09-01 00:00:00","2020-10-01 00:00:00","2014-05-01 00:00:00","2014-06-01 00:00:00","2014-07-01 00:00:00"],
        "agrup": ["('326003376', 'FG')","('326003376', 'FG')","('326003376', 'FG')","('326028183', 'FG')","('326028183', 'FG')","('326028183', 'FG')"],
        "group_seg": ["fg_g2","fg_g2","fg_g2","fg_g2","fg_g2","fg_g2"],
        "volume_carteira": [284.0,280.0,222.0,758.0,672.0,646.0],
        "origin_volume_carteira": [284.0,280.0,222.0,758.0,672.0,646.0],
        "custo_unitario": [58.355,58.355,58.355,8.004366472,8.004366472,8.004366472],
        "len_ts": [103,103,103,103,103,103]
    })

    dtypes = [
        ("level", np.int64), ("periodo", np.datetime64), ("agrup", str), ("group_seg", str),
        ("volume_carteira", np.float64), ("origin_volume_carteira", np.float64), ("custo_unitario", np.float64),
        ("len_ts", np.int64)
    ]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def crafted_estimators():
    df = pd.DataFrame({
        "agrup": ["('326003376', 'FG')", "('326028183', 'FG')"],
        "estimator": ["ARIMA(order=(1, 1, 1), seasonal_order=(1, 0, 0, 2))",
                      "XGBForecaster(lags=6, n_jobs=1, poly_degree=2)"],
        "rmse": [81.14210644866712, 311.9025082086742],
        "mape": [43.11558063925581, 44.01320476533838],
        "training_status": ["success", "success"],
        "error_message": [None, None],
        "metric": [22568.621832308683, 547742.6743609868],
        "param_order": ["(1, 1, 1)", None],
        "param_seasonal_order": ["(1, 0, 0, 2)", None],
        "param_damped_trend": [None, None],
        "param_seasonal": [None, None],
        "param_seasonal_periods": [None, None],
        "param_trend": [None, None],
        "param_poly_degree": [None, "2"],
        "param_n_estimators": [None, "412"],
        "param_min_iter_split": [None, None],
        "param_min_iter_leaf": [None, None],
        "param_max_depth": [None, "84"],
        "param_lags": [None, "6"],
        "param_colsample_bytree": [None, "0.408012591984218"],
        "param_gamma": [None, "0.2"],
        "param_learning_rate": [None, "0.09020376572803346"],
        "param_min_child_weight": [None, "1"],
        "param_n_jobs": [None, "1"],
        "param_alpha": [None, None]
    })

    dtypes = [
        ("agrup", str), ("estimator", str), ("rmse", np.float64), ("mape", np.float64),
        ("training_status", str), ("error_message", str), ("metric", np.float64),
        ("param_order", str), ("param_seasonal_order", str),
        ("param_damped_trend", str), ("param_seasonal", str),
        ("param_seasonal_periods", str), ("param_trend", str),
        ("param_poly_degree", str), ("param_n_estimators", str),
        ("param_min_iter_split",str), ("param_min_iter_leaf", str),
        ("param_max_depth", str), ("param_lags", str), ("param_colsample_bytree", str),
        ("param_gamma", str), ("param_learning_rate", str), ("param_min_child_weight", str),
        ("param_n_jobs", str), ("param_alpha", str)
    ]

    return convert_dtypes(df, dtypes)

