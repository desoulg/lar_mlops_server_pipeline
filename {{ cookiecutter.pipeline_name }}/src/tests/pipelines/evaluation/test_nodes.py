from src.{{cookiecutter.pipeline_name}}.pipelines.evaluation.nodes import evaluate_metrics
import pytest
import numpy as np

@pytest.fixture()
def evaluated_metrics(training_results):
    estimators, acc = evaluate_metrics(training_results)
    return estimators, acc

@pytest.fixture()
def estimator_results(evaluated_metrics, crafted_estimators):
    estimators, _ = evaluated_metrics
    return estimators.merge(crafted_estimators, on="agrup", how="inner", suffixes=("_l", "_r"))

@pytest.fixture()
def metric_results(evaluated_metrics, crafted_results):
    _, results = evaluated_metrics
    return results.merge(crafted_results, on="group_seg", how="inner", suffixes=("_l", "_r"))

def test_length_between_estimators_crafted_and_result(training_results, crafted_estimators):
    assert len(training_results) == len(crafted_estimators)

def test_rmse_values(estimator_results):
    assert estimator_results.rmse_l.equals(estimator_results.rmse_r)

def test_mape_values(estimator_results):
    assert estimator_results.mape_l.equals(estimator_results.mape_r)

def test_param_order_values(estimator_results):
    assert estimator_results.param_order_l.equals(estimator_results.param_order_r)

def test_param_seasonal_order_values(estimator_results):
    assert estimator_results.param_seasonal_order_l.equals(estimator_results.param_seasonal_order_r)

def test_metric_results_values(metric_results):
    assert np.allclose(metric_results.acuracia_l, metric_results.acuracia_r, atol=1e-6, equal_nan=False)

