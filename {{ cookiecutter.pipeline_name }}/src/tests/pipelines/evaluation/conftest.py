import pandas as pd
import numpy as np
import pytest
from ..utils import convert_dtypes


@pytest.fixture(scope="module")
def training_results():
    df = pd.DataFrame(
        {
            "agrup": ["('W10902870', 'DG')", "('W10827973', 'FG')", "('326047714', 'FG')", "('W10351342', 'DG')"],
            "level": [1, 1, 1, 1],
            "group_seg": ["dg_g2", "fg_g1", "fg_g2", "dg_g1"],
            "custo_unitario": [411.10030303, 134.023567901, 7.940734177, 61.248335788],
            "period": ["2020-05-01 00:00:00", "2021-04-01 00:00:00", "2019-06-01 00:00:00", "2020-09-01 00:00:00"],
            "preds": [7.0, 105.0, 228.0, 57.0],
            "y_test": [2.0, 239.0, 293.0, 183.0],
            "fold": [2, 12, 5, 17],
            "n_pred": [2, 5, 2, 5],
            "estimator": ["ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))"],
            "training_status": ["success", "success", "success", "success"],
            "error_message": [None, None, None, None],
            "metric": [1259.6704418605884, 1825.6603849256555, 24534.36473580352, 1200.9630889093125],
            "param_order": ["(0, 0, 1)", "(0, 0, 1)", "(0, 0, 1)", "(0, 0, 1)"],
            "param_seasonal_order": ["(0, 0, 0, 0)", "(0, 0, 0, 0)", "(0, 0, 0, 0)", "(0, 0, 0, 0)"]
        }
    )

    dtypes = [
        ("agrup", str), ("level", int), ("group_seg", str), ("custo_unitario", float),
        ("period", np.datetime64), ("preds", float), ("y_test", float), ("fold", int),
        ("n_pred", int), ("estimator", str), ("training_status", str), ("error_message", str),
        ("metric", float), ("param_order", str), ("param_seasonal_order", str)
    ]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def crafted_estimators():
    df = pd.DataFrame(
        {
            "agrup": ["('W10827973', 'FG')", "('W10902870', 'DG')", "('326047714', 'FG')", "('W10351342', 'DG')"],
            "estimator": ["ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))",
                          "ARIMA(order=(0, 0, 1), seasonal_order=(0, 0, 0, 0))"],
            "rmse": [134.0, 5.0, 65.0, 126.0],
            "mape": [56.06694560669456, 250.0, 22.18430034129693, 68.85245901639344],
            "training_status": ["success", "success", "success", "success"],
            "error_message": [None, None, None, None],
            "metric": [1825.6603849256555, 1259.6704418605884, 24534.36473580352, 1200.9630889093123],
            "param_order": ["(0, 0, 1)", "(0, 0, 1)", "(0, 0, 1)", "(0, 0, 1)"],
            "param_seasonal_order": ["(0, 0, 0, 0)", "(0, 0, 0, 0)", "(0, 0, 0, 0)", "(0, 0, 0, 0)"]
        }
    )

    dtypes = [
        ("agrup", str), ("estimator", str), ("rmse", float),
        ("mape", float), ("training_status", str), ("error_message", str), ("metric", float),
        ("param_order", str), ("param_seasonal_order", str)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def crafted_results():
    return pd.DataFrame(
        {
            "group_seg": ["dg_g2", "fg_g2"],
            "acuracia": [0.285714, 0.714912]
        }
    )
