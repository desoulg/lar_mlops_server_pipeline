from src.{{cookiecutter.pipeline_name}}.pipelines.preprocess_pipeline.nodes import prepare_time_series, forecastability, ts_segmentation, prepare_training_file


def test_prepare_time_series(filtered_abt, prepare_time_series_params, prepared_data):
    prepared_data_result = prepare_time_series(filtered_abt, **prepare_time_series_params)
    merged_df = prepared_data_result.merge(prepared_data, on=["periodo", "agrup_col"], suffixes=("_l", "_r"))

    assert len(merged_df) == len(prepared_data)
    assert merged_df.volume_carteira_l.equals(merged_df.volume_carteira_r)
    assert merged_df.custo_unitario_l.equals(merged_df.custo_unitario_r)


def test_forecastability(prepared_data, forecastability_params, data, components):
    data_result, components_result = forecastability(prepared_data, **forecastability_params)

    final_data = data_result.merge(data, on="agrup", suffixes=("_l", "_r"))
    final_components = components_result.merge(components, on=["agrup", "periodo"], suffixes=("_l", "_r"))

    assert len(final_data) == len(data_result)
    assert final_data.volume_12m_l.equals(final_data.volume_12m_r)
    assert final_data.custo_12m_l.equals(final_data.custo_12m_r)

    assert len(final_components) == len(components_result)
    assert final_components.seasonal_l.equals(final_components.seasonal_r)
    assert final_components.trend_l.equals(final_components.trend_r)


def test_ts_segmentation(data, ts_segmentation_params, seg):
    seg_result = ts_segmentation(data, **ts_segmentation_params)
    final_seg = seg_result.merge(seg, on="agrup", suffixes=("_l", "_r"))

    assert len(final_seg) == len(seg_result)
    assert final_seg.level_l.equals(final_seg.level_r)
    assert final_seg.group_seg_l.equals(final_seg.group_seg_r)


def test_prepare_training_file(seg, components, prepare_training_file_params, training_data):
    training_data_result = prepare_training_file(seg, components, **prepare_training_file_params)

    final_training_data = training_data_result.merge(training_data, on=["agrup", "periodo"], suffixes=("_l", "_r"))

    assert len(final_training_data) == len(training_data_result)
    assert final_training_data.level_l.equals(final_training_data.level_r)
    assert final_training_data.len_ts_l.equals(final_training_data.len_ts_r)