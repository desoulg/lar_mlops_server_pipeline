import pandas as pd
import pytest
import numpy as np

from src.tests.pipelines.utils import convert_dtypes


@pytest.fixture(scope="module")
def prepare_time_series_params():
    return {
        "agrup": ["material", "tipo_faturamento"],
        "agrup_target": None,
        "level_target": None,
        "levels": ["level1"],
        "top_500": False
    }


@pytest.fixture(scope="module")
def forecastability_params():
    return {
        "agrup": ["material", "tipo_faturamento"]
    }


@pytest.fixture(scope="module")
def ts_segmentation_params():
    return {
        "level": 1
    }

@pytest.fixture(scope="module")
def prepare_training_file_params():
    return {
        "level": 1,
        "training_groups": ["npi_level1"],
        "sampling": False,
        "n_samples": 10,
        "random_seed": 42
    }


@pytest.fixture(scope="module")
def filtered_abt():
    df = pd.DataFrame({
            "tipo_faturamento": ["FG","FG","FG","FG","FG","FG"],
            "periodo": ["2018-10-01 00:00:00","2018-11-01 00:00:00","2018-12-01 00:00:00","2016-09-01 00:00:00","2016-10-01 00:00:00","2016-11-01 00:00:00"],
            "mes": [10,11,12,9,10,11],
            "ano": [2018,2018,2018,2016,2016,2016],
            "mesano": ["102018","112018","122018","092016","102016","112016"],
            "material": ["W10841027","W10841027","W10841027","W10308284","W10308284","W10308284"],
            "descricao": ["PORTA EMB EXT REFRIG INOX","PORTA EMB EXT REFRIG INOX","PORTA EMB EXT REFRIG INOX","KIT DE INSTALACAO","KIT DE INSTALACAO","KIT DE INSTALACAO"],
            "categoria": ["REFRIGERADORES","REFRIGERADORES","REFRIGERADORES","REFRIGERADORES","REFRIGERADORES","REFRIGERADORES"],
            "negocio": ["REFRIGERAÇÃO","REFRIGERAÇÃO","REFRIGERAÇÃO","REFRIGERAÇÃO","REFRIGERAÇÃO","REFRIGERAÇÃO"],
            "subfamilia": ["PORTAS","PORTAS","PORTAS","KITS INSTALAÇÃO","KITS INSTALAÇÃO","KITS INSTALAÇÃO"],
            "familia": ["ESTRUTURAL","ESTRUTURAL","ESTRUTURAL","ESPECÍFICOS","ESPECÍFICOS","ESPECÍFICOS"],
            "origem": ["IMPORTADO","IMPORTADO","IMPORTADO","PLANTA","PLANTA","PLANTA"],
            "mercado": ["CATIVO","CATIVO","CATIVO","CATIVO","CATIVO","CATIVO"],
            "linha": ["DIVERSOS","DIVERSOS","DIVERSOS","INSTALAÇÃO","INSTALAÇÃO","INSTALAÇÃO"],
            "estrategia_portfolio": ["ESTÉTICOS","ESTÉTICOS","ESTÉTICOS","CONSUMÍVEIS E INSTALAÇÃO","CONSUMÍVEIS E INSTALAÇÃO","CONSUMÍVEIS E INSTALAÇÃO"],
            "custo_unitario": ["956.338333333","956.338333333","956.338333333","57.145000000","57.145000000","57.145000000"],
            "demanda_projetada": [None,None,None,None,None,None],
            "top_500": ["NÃO","NÃO","NÃO","NÃO","NÃO","NÃO"],
            "volume_carteira": [1.000000000,None,None,5.000000000,18.000000000,16.000000000],
        })

    dtypes = [("tipo_faturamento", str), ("periodo", np.datetime64), ("mes", int), ("ano", int),
             ("mesano", str), ("material", str), ("descricao", str), ("categoria", str),
             ("negocio", str), ("subfamilia", str), ("familia", str), ("origem", str),
             ("mercado", str), ("linha", str), ("estrategia_portfolio", str),
             ("custo_unitario", str), ("demanda_projetada", str), ("top_500", str),
             ("volume_carteira", str)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def prepared_data():
    df = pd.DataFrame({
        "periodo": ["2016-09-01", "2016-10-01", "2016-11-01", "2018-10-01", "2018-11-01", "2018-12-01"],
        "material": ["W10308284", "W10308284", "W10308284", "W10841027", "W10841027", "W10841027"],
        "tipo_faturamento": ["FG", "FG", "FG", "FG", "FG", "FG"],
        "volume_carteira": [5.0, 18.0, 16.0, 1.0, 1.0, 0.0],
        "custo_unitario": [57.145, 57.145, 57.145, 956.338333333, 956.338333333, 956.338333333],
        "origin_volume_carteira": [5.0, 18.0, 16.0, 1.0, 0.0, 0.0],
        "agrup_col": ["('W10308284', 'FG')", "('W10308284', 'FG')", "('W10308284', 'FG')", "('W10841027', 'FG')",
                      "('W10841027', 'FG')", "('W10841027', 'FG')"]
    })

    dtypes = [("periodo", np.datetime64), ("material", str), ("tipo_faturamento", str),
             ("volume_carteira", float), ("custo_unitario", float), ("origin_volume_carteira", float),
             ("agrup_col",
              str)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def components():
    df = pd.DataFrame({
        "periodo": ["2016-09-01 00:00:00", "2016-10-01 00:00:00", "2016-11-01 00:00:00", "2018-10-01 00:00:00",
                    "2018-11-01 00:00:00", "2018-12-01 00:00:00"],
        "material": ["W10308284", "W10308284", "W10308284", "W10841027", "W10841027", "W10841027"],
        "tipo_faturamento": ["FG", "FG", "FG", "FG", "FG", "FG"],
        "volume_carteira": [5.0, 18.0, 16.0, 1.0, 1.0, 0.0],
        "custo_unitario": [57.145, 57.145, 57.145, 956.338333333, 956.338333333, 956.338333333],
        "origin_volume_carteira": [5.0, 18.0, 16.0, 1.0, 0.0, 0.0],
        "seasonal": [None, None, None, None, None, None],
        "trend": [None, None, None, None, None, None],
        "resid": [None, None, None, None, None, None],
        "agrup": ["('W10308284', 'FG')", "('W10308284', 'FG')", "('W10308284', 'FG')", "('W10841027', 'FG')",
                  "('W10841027', 'FG')", "('W10841027', 'FG')"],
    })

    dtypes = [("periodo", np.datetime64), ("material", str), ("tipo_faturamento", str),
             ("volume_carteira", float), ("custo_unitario", float), ("origin_volume_carteira", float),
             ("seasonal", float), ("trend", float), ("resid", float), ("agrup", str)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def data():
    df = pd.DataFrame({
        "volume_12m": [39.0, 1.0],
        "custo_12m": [2228.655, 956.338333333],
        "first_point": [1, 1],
        "last_point": [3, 2],
        "len_ts": [3, 2],
        "cv": [0.4396520051149294, 0.7071067811865476],
        "sampen": [1.0, 1.0],
        "agrup": ["('W10308284', 'FG')", "('W10841027', 'FG')"],
        "custo_12m_fat": [3184.993333333, 3184.993333333],
        "cost_percent": [0.699736158526831, 0.300263841473169],
        "cumsum_cost": [0.699736158526831, 1.0],
    })

    dtypes = [("volume_12m", float), ("custo_12m", float), ("first_point", int), ("last_point", int),
             ("len_ts", int), ("cv", float), ("sampen", float), ("agrup", str),
             ("custo_12m_fat", float), ("cost_percent", float), ("cumsum_cost", float)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def seg():
    df = pd.DataFrame({
            "volume_12m": [39.0,1.0],
            "custo_12m": [2228.655,956.338333333],
            "first_point": ["1","1"],
            "last_point": ["3","2"],
            "len_ts": ["3","2"],
            "cv": [0.4396520051149294,0.7071067811865476],
            "sampen": [1.0,1.0],
            "agrup": ["('W10308284', 'FG')","('W10841027', 'FG')"],
            "custo_12m_fat": [3184.993333333,3184.993333333],
            "cost_percent": [0.699736158526831,0.300263841473169],
            "cumsum_cost": [0.699736158526831,1.0],
            "level": [1,1],
            "group_seg": ["npi_level1","npi_level1"]
    })

    dtypes = [("volume_12m", float), ("custo_12m", float), ("first_point", np.int32), ("last_point", np.int32),
              ("len_ts", np.int32), ("cv", float), ("sampen", float), ("agrup", str),
              ("custo_12m_fat", float), ("cost_percent", float), ("cumsum_cost", float), ("level", np.int64),
              ("group_seg", str)]

    return convert_dtypes(df, dtypes)


@pytest.fixture(scope="module")
def training_data():
    df = pd.DataFrame({
        "level": [1, 1, 1, 1, 1, 1],
        "periodo": ["2016-09-01 00:00:00", "2016-10-01 00:00:00", "2016-11-01 00:00:00", "2018-10-01 00:00:00",
                    "2018-11-01 00:00:00", "2018-12-01 00:00:00"],
        "agrup": ["('W10308284', 'FG')", "('W10308284', 'FG')", "('W10308284', 'FG')", "('W10841027', 'FG')",
                  "('W10841027', 'FG')", "('W10841027', 'FG')"],
        "group_seg": ["npi_level1", "npi_level1", "npi_level1", "npi_level1", "npi_level1", "npi_level1"],
        "volume_carteira": [5.0, 18.0, 16.0, 1.0, 1.0, 0.0],
        "origin_volume_carteira": [5.0, 18.0, 16.0, 1.0, 0.0, 0.0],
        "custo_unitario": [57.145, 57.145, 57.145, 956.338333333, 956.338333333, 956.338333333],
        "len_ts": [3, 3, 3, 2, 2, 2]
    })

    dtypes = [("level", np.int64), ("periodo", np.datetime64), ("agrup", str), ("group_seg", str),
             ("volume_carteira", float), ("origin_volume_carteira", float), ("custo_unitario", float),
             ("len_ts", np.int32)]

    return convert_dtypes(df, dtypes)

