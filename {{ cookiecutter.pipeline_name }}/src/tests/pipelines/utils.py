import pandas as pd

def convert_dtypes(df, dtypes):
    for c, j in dtypes:
        df[c] = df[c].astype(j)

    return df