def clean_name(name):
    name = name.replace("(", "")
    name = name.replace(")", "")
    name = name.replace(", ", "_")
    name = name.replace("'", "")
    name = name.replace(" ", "")
    return name