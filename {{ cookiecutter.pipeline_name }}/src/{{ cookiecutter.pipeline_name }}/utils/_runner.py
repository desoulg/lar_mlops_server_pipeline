from kedro.runner import SequentialRunner, ParallelRunner
from kedro.io import AbstractDataSet, DataCatalog, MemoryDataSet
from kedro.extras.datasets.pandas import ParquetDataSet
from multiprocessing.managers import SyncManager
from kedro.pipeline import Pipeline
from typing import Any, Dict, Iterable
from kedro.framework.hooks import get_hook_manager

class CustomSequentialRunner(SequentialRunner):
    
    def create_series_data_set(self, ds_name: str):
        if ds_name.startswith("serie_id:"):
            return MemoryDataSet(data=ds_name.replace("serie_id:", ""))
        if ds_name.startswith("split_id:"):
            value = ds_name.replace("split_id:", "")
            value = int(value) if value.isdigit() else value
            return MemoryDataSet(data=value)

        return MemoryDataSet()
    
    def create_output_data_set(self, ds_name: str):
        hook_manager = get_hook_manager()
        filepath = hook_manager.hook.before_output_register(  # pylint: disable=no-member
            ds_name=ds_name
        )
        filepath = filepath if isinstance(filepath, str) else filepath[0]
        
        return ParquetDataSet(filepath=filepath)

    def run(
        self, pipeline: Pipeline, catalog: DataCatalog, run_id: str = None
    ) -> Dict[str, Any]:
        """Run the ``Pipeline`` using the ``DataSet``s provided by ``catalog``
        and save results back to the same objects.
        Args:
            pipeline: The ``Pipeline`` to run.
            catalog: The ``DataCatalog`` from which to fetch data.
            run_id: The id of the run.
        Raises:
            ValueError: Raised when ``Pipeline`` inputs cannot be satisfied.
        Returns:
            Any node outputs that cannot be processed by the ``DataCatalog``.
            These are returned in a dictionary, where the keys are defined
            by the node outputs.
        """

        catalog = catalog.shallow_copy()

        unsatisfied = pipeline.inputs() - set(catalog.list())
        unregistered_ds = pipeline.data_sets() - set(catalog.list())

        for ds_name in unsatisfied:
            if (ds_name.startswith("serie_id:") or ds_name.startswith("split_id:")):
                catalog.add(ds_name, self.create_series_data_set(ds_name))
        for ds_name in unregistered_ds:
            if not (ds_name.startswith("serie_id:") or ds_name.startswith("split_id:")):
                catalog.add(ds_name, self.create_output_data_set(ds_name))
        return super().run(pipeline, catalog, run_id)   


class _SharedMemoryDataSet:
    """``_SharedMemoryDataSet`` a wrapper class for a shared MemoryDataSet in SyncManager.
    It is not inherited from AbstractDataSet class.
    """

    def __init__(self, manager: SyncManager, data=None):
        """Creates a new instance of ``_SharedMemoryDataSet``,
        and creates shared memorydataset attribute.
        Args:
            manager: An instance of multiprocessing manager for shared objects.
        """
        if data is not None:
            self.shared_memory_dataset = manager.MemoryDataSet(data=data)  # type: ignore
        else:
            self.shared_memory_dataset = manager.MemoryDataSet()

    def __getattr__(self, name):
        # This if condition prevents recursive call when deserializing
        if name == "__setstate__":
            raise AttributeError()
        return getattr(self.shared_memory_dataset, name)

    def save(self, data: Any):
        """Calls save method of a shared MemoryDataSet in SyncManager."""
        try:
            self.shared_memory_dataset.save(data)
        except Exception as exc:  # pylint: disable=broad-except
            # Checks if the error is due to serialisation or not
            try:
                pickle.dumps(data)
            except Exception as exc:  # SKIP_IF_NO_SPARK
                raise DataSetError(
                    f"{str(data.__class__)} cannot be serialized. ParallelRunner "
                    "implicit memory datasets can only be used with serializable data"
                ) from exc
            else:
                raise exc