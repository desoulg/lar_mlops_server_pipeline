from importlib import import_module

def model_from_string(model_name, default_args=None):
    model_class = getattr(
        import_module((".").join(model_name.split(".")[:-1])),
        model_name.rsplit(".")[-1],
    )
    if default_args is None:
        return model_class()
    else:
        return model_class(**default_args)