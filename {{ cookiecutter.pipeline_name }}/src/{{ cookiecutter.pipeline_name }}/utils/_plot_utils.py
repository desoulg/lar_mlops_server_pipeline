import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings("ignore")

def plot_in_sample_pred(pred):
    return plot_with_error_bounds(pred.index, pred.yhat, pred.yhat_upper, pred.yhat_lower)


def plot_with_error_bounds(
        x, y, upper_bound, lower_bound,
        fig=None, ax=None, figsize=None, dpi=100,
        line_color=[0.4]*3, shade_color=[0.7]*3,
        shade_alpha=0.5, linewidth=2.0, legend_loc='best',
        line_label='Data', shade_label='$\mathregular{\pm}$STD',
        logx=False, logy=False, grid_on=True,
):
    
    fig, ax = _process_fig_ax_objects(fig, ax, figsize, dpi)

    hl1 = ax.fill_between(
        x, lower_bound, upper_bound,
        color=shade_color, facecolor=shade_color,
        linewidth=0.01, alpha=shade_alpha, interpolate=True,
        label=shade_label,
    )
    hl2, = ax.plot(x, y, color=line_color, linewidth=linewidth, label=line_label)
    if logx: ax.set_xscale('log')
    if logy: ax.set_yscale('log')

    if grid_on:
        ax.grid(ls=':',lw=0.5)
        ax.set_axisbelow(True)

    plt.legend(handles=[hl2,hl1],loc=legend_loc)

    return fig, ax

def _process_fig_ax_objects(fig, ax, figsize=None, dpi=None, ax_proj=None):
    if fig is None:  # if a figure handle is not provided, create new figure
        fig = plt.figure(figsize=figsize,dpi=dpi)
    else:   # if provided, plot to the specified figure
        plt.figure(fig.number)

    if ax is None:  # if ax is not provided
        ax = plt.axes(projection=ax_proj)  # create new axes and plot lines on it
    else:
        ax = ax  # plot lines on the provided axes handle

    return fig, ax


    