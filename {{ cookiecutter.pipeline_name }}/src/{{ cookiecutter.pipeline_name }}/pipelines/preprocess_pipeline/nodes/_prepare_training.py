from typing import List
import pandas as pd
import logging
import warnings
import pandas as pd
import numpy as np

warnings.filterwarnings("ignore")
logger = logging.getLogger(__name__)

def prepare_training_file(
    df_seg: pd.DataFrame, 
    df_components: pd.DataFrame, 
    level: int,
    training_groups: List[str], 
    sampling: bool = False,
    n_samples: int = 10,
    random_seed: int = 0):
    """
    This node create a training file from segmentation e components data. This dataset will 
    contain all the series that will be used in training.
        Args:
            df_seg: forecastability with metrics and segmentation.
            df_components: prepared and decomposed parts of time-series.
            level: Level of disaggregation.
            sampling: If data sampling will be done.
            n_samples: Number of samples by segmentation group.
        Returns:
            pd.DataFrame -> Training File
    """
    logger.info(f"Used groups:{training_groups}")
    df_seg_comp = pd.merge(df_seg, df_components, how='left', on='agrup', validate='1:m')
    df_seg_comp = df_seg_comp[df_seg_comp.group_seg.isin(training_groups)]
    training_file = df_seg_comp[['level',
                                 'periodo',
                                 'agrup',
                                 'group_seg',
                                 'volume_carteira', 
                                 'origin_volume_carteira',
                                 'custo_unitario', 
                                 'len_ts']]
    
    training_file['periodo'] = pd.to_datetime(training_file.periodo)
    
    logger.info(f"Number of Series before sampling:{training_file.agrup.nunique()}")
    logger.info(f"Shape before sampling:{training_file.shape}")
    if sampling and training_file.agrup.unique().shape[0] > n_samples:
        training_file = sampling_training_file(training_file, training_groups, n=n_samples, random_seed=random_seed)
        logger.info(f"Number of Series after sampling:{training_file.agrup.nunique()}")
        logger.info(f"Shape after sampling:{training_file.shape}")
    
    test_training_file(training_file, level, training_groups)
     
    return training_file


def sampling_training_file(
    training_file: pd.DataFrame, 
    training_groups: List[str], 
    n: int,
    random_seed: int):
    """
    This function performs sampling by segmentation group.
        Args:
            training_file: Training File.
            training_groups: Groups in which sampling will be performed.
            n: Number of samples by segmentation group.
        Returns:
            pd.DataFrame -> Sampling Training File.
    """
    series = training_file.groupby('agrup').nunique().periodo
    series = series[series == series.max()]
    sampling = pd.DataFrame()
    np.random.seed(random_seed)
    for group in training_groups:
        df = training_file[training_file.group_seg == group]
        agrups = np.random.choice(df.agrup.unique(), size=min(n,df.agrup.unique().shape[0]) , replace=False)
        sampling = sampling.append(df[df.agrup.isin(agrups)], ignore_index=True)   
    return sampling


def test_training_file(
    training_file: pd.DataFrame, 
    level: int, 
    training_groups: List[str]):
    """
    This function tests the training file to ensure that it follows the necessary conditions for training. 
        Args:
            training_file: Training File.
            training_groups: Segmentation groups in training file.
            level: Level of disaggregation.
        Returns:
            None
    """
    logger.info("Testing training file...")
    cols_with_nan = training_file.columns[training_file.isna().any()].tolist()
    
    if cols_with_nan:
        raise ValueError(f'Invalid training file. NaN values were found in the following columns: {cols_with_nan}.')
        
    elif not training_file.level.unique().all() == level:
        raise ValueError(f"""Invalid training file. Current levels {training_file.level.unique().values}
        are in the training file but the pipeline supports only level {level}.""")
        
    elif not training_file.group_seg.unique().all() in training_groups:
        raise ValueError(f"""Invalid training file. Groups in the training file: {training_file.group_seg.unique()}
        do not match the specified groups for this level: {training_groups}.""")
        
    logger.info("Training file test completed successfully!") 
        