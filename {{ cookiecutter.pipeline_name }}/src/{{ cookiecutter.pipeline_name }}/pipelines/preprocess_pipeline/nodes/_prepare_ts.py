from {{cookiecutter.pipeline_name}}.utils import rolling_fill, parallel_groupby
from typing import List, Dict

import numpy as np
import pandas as pd
import warnings
import logging
import time 

logger = logging.getLogger(__name__)
warnings.filterwarnings("ignore")


def prepare_time_series(
    data: pd.DataFrame,
    agrup: List[str] = ['material', 'tipo_faturamento'],
    agrup_target: str = None,
    level_target: int = None,
    levels: Dict = None,
    top_500: bool = False,
    serie_col: str = 'volume_carteira',
    cost_col: str = 'custo_unitario',
    date_col: str = 'periodo',
    n: int = 2
    
    
) -> pd.DataFrame:
    
    """
    This Function prepares the time series to be used in modeling. 
    """
    
    start_time = time.time()
    data['volume_carteira'] = data.volume_carteira.astype("float64")
    
    # Get valid series
    valid_series = parallel_groupby(data, get_valid_series,group_cols=agrup)
    valid_series.name = "is_valid"
    data = pd.merge(data, valid_series, on=agrup)
    data  = data[data.is_valid].drop(columns=["is_valid"])
    
    # ensure correct grouping and sortment of the input data
    df = parallel_groupby(data, 
                          summarize_data, 
                          group_cols=agrup + [date_col],
                          serie_col=serie_col,
                          cost_col=cost_col
                         )
    
    df = df[[date_col] + agrup + [serie_col] + [cost_col]].sort_values(by = agrup + [date_col]).reset_index(drop=True)
    
    # fill missing values in cost_col
    df.loc[:, cost_col].fillna(value=0, inplace=True)
    
    # create column to save original time series values
    df['origin_'+serie_col] = df[serie_col].fillna(0)
    
    # Fill Nan values
    df_fill = parallel_groupby(df, 
                               time_series_knn_fillna, 
                               group_cols=agrup, 
                               serie_col=serie_col, 
                               n=n
                              )
    
    # Group and stacking series
    out = np.hstack(df_fill)
    
    # transfer result to existing dataframe
    df.loc[:, serie_col] = out
    
    
    logger.info(f"Prepare exec time:{(time.time() - start_time)/60:.2f} min")
    # return input data with corrected time series values
    return df

def get_valid_series(data, group_cols):
    return data.groupby(group_cols).apply(lambda x: ((~np.isnan(x.volume_carteira)).any()))

def time_series_knn_fillna(
    data: pd.DataFrame,
    group_cols: List[str],
    serie_col: str,
    n: int
) -> pd.DataFrame:
    knn_fill = data.groupby(group_cols, sort=False).apply(lambda x: rolling_fill(data = x[serie_col],
                                                                                 n = n))
    
    return knn_fill


def summarize_data(
    data: pd.DataFrame,
    group_cols: List[str],
    serie_col: str,
    cost_col: str
) -> pd.DataFrame:
    
    summarized_data = data.groupby(group_cols, sort=False, as_index=False).agg({serie_col: lambda x: x.sum(min_count=1), cost_col: 'mean'})
    
    return summarized_data

