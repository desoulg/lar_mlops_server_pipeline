from typing import List, Dict, Optional

import numpy as np
import pandas as pd
import warnings
import logging
import time 

logger = logging.getLogger(__name__)
warnings.filterwarnings("ignore")

def filter_abt(
    data: pd.DataFrame,
    date_filter: Dict,
    validated_data: Optional[pd.DataFrame]  = None) -> pd.DataFrame:
    #data['material'] = data.material.astype(str)
    #data['volume_carteira'] = data.volume_carteira.astype("float64")
    data["periodo"] = pd.to_datetime(data["periodo"])
    return data[getattr(data["periodo"].dt.date, date_filter['method'])(date_filter['date'])]