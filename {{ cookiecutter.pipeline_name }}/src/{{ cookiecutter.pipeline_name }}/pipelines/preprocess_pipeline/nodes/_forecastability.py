from typing import List
from {{cookiecutter.pipeline_name}}.utils import parallel_groupby
from statsmodels.tsa.seasonal import seasonal_decompose
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')

def forecastability(
    data: pd.DataFrame, 
    agrup: List[str] = ['material', 'tipo_faturamento'],
    serie_col: str = 'volume_carteira',
    cost_col: str = 'custo_unitario',
    date_col: str = 'periodo',
    seasonal_period: int = 12
) -> (pd.DataFrame, pd.DataFrame):
    
    """ Perform forecastability. The goal is to extract metrics for each time-serie,
    understanding patterns for further clusterization. Also performs time-series 
    decomposition. Metrics = (Sample Entropy, Coefficient of Variance, first point,
    las point, time series lenght, sales volume and cost in the last 12 months, 
    cumulative cost). Coeffient of Variance is calculated in deseasonalized time-serie.
    
        Args:
            data: pd.Dataframe, prepared data containing time-series.
            agrup: list, columns for aggregation.
            serie_col: str, column name containing time-series.
            cost_col: str, column name containing costs.
            date_col: str, column name containing date.
            seasonal_period: int, seasonal period to be considered for 
                             time-series decomposition.
        Returns:
            pd.DataFrame -> forecastability with metrics;
            pd.DataFrame -> original, prepared and decomposed parts of time-series;
    """
    
    if data.volume_carteira.isna().any():
        raise ValueError("Some series contain NA values!")
    
    df_components = pd.DataFrame()
    # loop througth time series
    df_forecastability = parallel_groupby(data, 
                                          forecastability_group, 
                                          group_cols=agrup,
                                          serie_col=serie_col, 
                                          cost_col=cost_col, 
                                          seasonal_period=seasonal_period)
    
    df_components = pd.concat(df_forecastability.components.values, ignore_index=True)
    df_forecastability = df_forecastability.drop(columns=['components'])
    
    # SampEn miscalculations set to 1
    df_forecastability['sampen'] = df_forecastability.sampen.fillna(1)
    
    # SampEn greater than 10 set to 10
    df_forecastability['sampen'] = np.where(df_forecastability.sampen > 10, 10, df_forecastability.sampen)
    df_forecastability = df_forecastability.reset_index()
    if len(agrup) > 1:
        df_forecastability['agrup'] = list(map(str, zip(*[df_forecastability[c] for c in agrup])))
        df_components['agrup'] = list(map(str, zip(*[df_components[c] for c in agrup])))
    else:
        df_forecastability['agrup'] = df_forecastability[agrup]
        df_components['agrup'] = df_components[agrup]
    # if 'tipo_faturamento' is present (level_1), then cumulative cost is calculated within 'FG' and 'DG' groups
    if 'tipo_faturamento' in agrup:
    
        # map agrup to str
        df_forecastability['agrup_s'] = df_forecastability['agrup'].map(str)
        
        # create column 'tipo_faturamento' inside df_forecastability
        df_forecastability['tipo_faturamento'] = np.where(df_forecastability.agrup_s.str.contains('FG', na=False), 'FG', 'DG')
        
        # calculate sum of cost in the last 12 months
        df_cost = df_forecastability.groupby(['tipo_faturamento'],
                                             sort=False,
                                             as_index=False).agg(custo_12m_fat=('custo_12m', 'sum'))
    
        # merge calculated cost with df_forecastability
        df_forecastability = pd.merge(df_forecastability, df_cost, how='left', on='tipo_faturamento')
        
        # calculate % cost
        df_forecastability['cost_percent'] = df_forecastability.custo_12m/df_forecastability.custo_12m_fat
        
        # sort df_forecastability with descending cost_percent
        df_forecastability.sort_values(['tipo_faturamento', 'cost_percent'], ascending=False, inplace=True)
        
        # calculate cumulative cost
        df_forecastability['cumsum_cost'] = df_forecastability.groupby('tipo_faturamento', sort=False)['cost_percent'].cumsum()
        
        # drop columns 'agrup_s', 'tipo_faturamento'
        df_forecastability.drop(columns=['agrup_s', 'tipo_faturamento'], inplace=True)
        
    else: # level_0 does not need cumulative cost to be calculated within groups
        
        # calculate % cost
        df_forecastability['cost_percent'] = df_forecastability.custo_12m/df_forecastability.custo_12m.sum()
        
        # sort df_forecastability with descending cost_percent
        df_forecastability.sort_values(['cost_percent'], ascending=False, inplace=True)
        
        # calculate cumulative cost
        df_forecastability['cumsum_cost'] = df_forecastability.cost_percent.cumsum()
        
    # return df_forecatability and df_components
    return df_forecastability.reset_index(drop=True).drop(columns='material'), df_components.drop(columns='agrup_col')

def forecastability_group(
        data: pd.DataFrame,
        group_cols: List[str],
        serie_col: str,
        cost_col: str,
        seasonal_period: int
    ) -> pd.DataFrame:
        
        """
        
        """
        grouped_data = data.groupby(group_cols, sort=False)
        grouped_forecastability = grouped_data.apply(lambda x: ts_components_sampen(data=x,
                                                                                    serie_col=serie_col,
                                                                                    cost_col=cost_col,
                                                                                    seasonal_period=seasonal_period))
    
        return grouped_forecastability

def ts_components_sampen(
            data: pd.DataFrame,
            serie_col: str,
            cost_col: str,
            seasonal_period: int
        ) -> pd.Series:
            
        """

        """

        # extract adjusted time series values (came from 'prepare_time_series')
        ts = data[serie_col].values

        # extract original time series values
        pure_ts = data['origin_'+serie_col].values

        # extract cost values
        cost = data[cost_col].values * pure_ts

        # indices of nonzero elements
        nonzeros = np.nonzero(ts)

        # get indice where time serie starts
        first_point = nonzeros[0][0]

        # get indice where time series ends
        last_point = nonzeros[0][-1]

        # calculate time series length
        len_ts = (last_point - first_point) + 1

        # update time series cutting off zeros in the beginning
        ts = ts[first_point:]

        # calculating sample entropy
        sampen = SampEn(ts, m=2, r=0.2*np.std(ts))        

        # calculating deseasonalized volume
        if  ts.size < (2 * seasonal_period): # in the case of poor data, seasonal, trend and resid set to np.nan

            # seasonal, trend and resid
            seasonal = trend = resid = np.empty(ts.size)
            seasonal[:] = trend[:] = resid[:] = np.nan

            # deseasonalized time serie equals to original
            des_vol = ts

        elif np.any(ts == 0): # if there's any 0 in volume, the seasonallity should be additive

            # components
            components = seasonal_decompose(ts, model='additive', period=seasonal_period)

            # seasonal, trend and resid
            seasonal = components.seasonal
            trend = components.trend
            resid = components.resid

            # deseasonalized time serie
            des_vol = ts - seasonal

        # else, seasonallity should be multiplicative      
        else: 

            # components
            components = seasonal_decompose(ts, model='multiplicative', period=seasonal_period)

            # seasonal, trend and resid
            seasonal = components.seasonal
            trend = components.trend
            resid = components.resid

            # deseasonalized time serie
            des_vol = ts/seasonal

        # calculating mean of deseasonalized time serie
        des_vol_mean = des_vol.mean()

        cv = np.nan
        if des_vol_mean != 0: # since cv is only defined for nonzero values

            # calculate coefficient of variance
            cv = (des_vol.std())/(des_vol_mean)

            if des_vol_mean < 0:
                cv = 1

        df_components = data.reset_index(drop=True).iloc[first_point:, :]
        df_components.loc[first_point:, 'seasonal'] = seasonal
        df_components.loc[first_point:, 'trend'] = trend
        df_components.loc[first_point:, 'resid'] = resid

        return pd.Series(
            {'volume_12m': np.round(pure_ts[-12:].sum()),
             'custo_12m': cost[-12:].sum(),
             'first_point': first_point + 1,
             'last_point': last_point + 1,
             'len_ts': len_ts,
             'cv': cv,
             'sampen': sampen,
             'components': df_components
            }
        )

def SampEn(
    L: np.array,
    m: int,
    r: int
) -> int:
    
    """ Calculates Sample Entropy for a given time series. Sample entropy (SampEn)
    is a modification of approximate entropy (ApEn), used for assessing the 
    complexity of time-series signals. For more details please refer to 
    https://www.mdpi.com/1099-4300/21/6/541.
    
        Args:
            L: array_like, time-series signal.
            m: int, embedding dimension.
            r: int, tolerance
        Returns:
            int, sample entropy.
    """
    # Initialize parameters
    N = len(L)
    B = 0.0
    A = 0.0
    
    # Split time series and save all templates of length m
    xmi = np.array([L[i : i + m] for i in range(N - m)])
    xmj = np.array([L[i : i + m] for i in range(N - m + 1)])

    # Save all matches minus the self-match, compute B
    B = np.sum([np.sum(np.abs(xmii - xmj).max(axis=1) <= r) - 1 for xmii in xmi])

    # Similar for computing A
    m += 1
    xm = np.array([L[i : i + m] for i in range(N - m + 1)])

    A = np.sum([np.sum(np.abs(xmi - xm).max(axis=1) <= r) - 1 for xmi in xm])

    # Return SampEn
    return -np.log(A / B)

def ts_segmentation(data: pd.DataFrame, level: int) -> (pd.DataFrame):
    
    """ Time-series segmentation. Applies segmentation rules according 
    to the relative importance of each time-serie for the business 
    (cumulative cost) and the difficulty to perform forecasts (Sample Entropy). 
    
        Args:
            data: pd.Dataframe. Results of forecastability process.
        Returns:
            pd.DataFrame -> Same structure as the input dataframe, 
                            but containing hierarchic level to forecasts
                            and segmentation results.

    """
    
    # create copy of input data (results of forecastability)
    df_seg = data.copy()
    
    # map agrup to str
    df_seg['agrup'] = df_seg['agrup'].map(str)
    
    # create column named 'level'. If FG or DG present in agrup then level 1 else 0
    #df_seg['level'] = np.where(df_seg.agrup.str.contains('FG', na=False) | df_seg.agrup.str.contains('DG', na=False), 1, 0)
    df_seg['level'] = level
    
    # stablish npi point. Always 12 months before the max cutoff date
    npi_point = df_seg.last_point.max() - 12
    
    # create column named group_seg and setting 'g0' as the default value
    df_seg['group_seg'] = 'g0'

    # if the input data has level 1 granularity then rules are applied considering FG and DG groups
    if np.any(df_seg.level == 1):
    
        # create groups according to 'tipo_faturamento', 'volume_12m', 'cumsum_cost' and sampen
        conditions = {'fg_g1': (df_seg.agrup.str.contains('FG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen <= 0.7),
                      'fg_g2': (df_seg.agrup.str.contains('FG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen > 0.7),
                      'fg_g3': (df_seg.agrup.str.contains('FG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen <= 0.7),
                      'fg_g4': (df_seg.agrup.str.contains('FG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen > 0.7),
                      'dg_g1': (df_seg.agrup.str.contains('DG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen <= 0.7),
                      'dg_g2': (df_seg.agrup.str.contains('DG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen > 0.7),
                      'dg_g3': (df_seg.agrup.str.contains('DG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen <= 0.7),
                      'dg_g4': (df_seg.agrup.str.contains('DG', na=False)) & (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen > 0.7),
                      'npi_level1': (df_seg.first_point > npi_point)}       
            
    elif np.any(df_seg.level == 0): # conditions for level 0 granularity
    
        # create groups according to 'volume_12m', 'cumsum_cost' and sampen
        conditions = {'g1': (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen <= 0.7),
                      'g2': (df_seg.volume_12m > 0) & (df_seg.cumsum_cost <= 0.9) & (df_seg.sampen > 0.7),
                      'g3': (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen <= 0.7),
                      'g4': (df_seg.volume_12m > 0) & (df_seg.cumsum_cost > 0.9) & (df_seg.sampen > 0.7),
                      'npi_level0': (df_seg.first_point > npi_point)}
        
    else: # in the future it is possible to create other granularities
        raise NotImplementedError("Level conditions are not implemented.")
    
    # applying conditions and update column 'group_seg'
    for key, c in conditions.items():
        df_seg['group_seg'] = np.where(c, key, df_seg.group_seg)
    
    # return df_seg, which is a copy of the input data + columns 'group_seg' and 'level'
    return df_seg