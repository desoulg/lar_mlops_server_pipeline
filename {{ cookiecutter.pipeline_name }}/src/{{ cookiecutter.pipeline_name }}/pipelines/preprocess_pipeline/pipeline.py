from functools import reduce
from operator import add

from kedro.pipeline import Pipeline, node
from kedro.pipeline.modular_pipeline import pipeline
from kedro.framework.session import get_current_session

from .nodes import prepare_time_series, forecastability, filter_abt


def create_filter_abt_pipeline():
    return Pipeline([
        node(
            func=filter_abt,
            inputs=["abt",
                    "params:date_filter",
                    "validated_abt"],
            outputs="filtered_abt",
            name="filter_abt"
        ),
    ])


def create_preprocessing_pipelines(levels) -> Pipeline:
    def _pipeline(level):
        return Pipeline([
            node(
                func=prepare_time_series,
                inputs=["filtered_abt",
                        "params:agrup",
                        "params:agrup_target",
                        "params:level_target",
                        "params:levels",
                       "params:top_500"],
                outputs="prepared_data",
                name=f"prepare_time_series_{level}"
            ),

            node(
                func=forecastability,
                inputs=["prepared_data", "params:agrup"],
                outputs=["forecastability", "components"],
                name=f"forecastablity_{level}"
            ),

            node(
                func=ts_segmentation,
                inputs=["forecastability",
                        "params:level_id"],
                outputs="seg",
                name=f"ts_segmentation_{level}"
            ),
            node(
                func=prepare_training_file,
                inputs=["seg",
                        "components",
                        "params:level_id",
                        "params:groups" ,
                        "params:abt_sampling",
                        "params:n_samples",
                       "params:random_seed"],
                outputs=f"training_data",
                name=f"prepare_training_file_{level}"
            ),
        ])

    combined_pipelines = reduce(add, [
        pipeline(
            pipe=_pipeline(level),
            parameters={
                "params:level_id": f"params:levels.{level}.id",
                "params:agrup": f"params:levels.{level}.agrup",
            },
            inputs={"filtered_abt": "filtered_abt"},
            outputs={  # both of these are tracked as experiments
                "prepared_data": f"prepared_data_{level}",
                "components": f"components_{level}",
                "forecastability": f"forecastability_{level}",
            }
        )
        for level in levels
    ])

    return pipeline(
        pipe=combined_pipelines,
        inputs=["filtered_abt"])


def create_pipeline() -> Pipeline:
    session = get_current_session()
    context = session.load_context()
    catalog = context.catalog

    levels = catalog.load("params:levels")
    level_target = catalog.load("params:level_target")
    if level_target is not None:
        levels = list(filter(lambda x: x == level_target, levels))
    
    filter_pipeline = create_filter_abt_pipeline()
    # Instantiate a new modeling pipeline for every model type
    preprocessing_pipelines = create_preprocessing_pipelines(levels)

    return filter_pipeline + preprocessing_pipelines