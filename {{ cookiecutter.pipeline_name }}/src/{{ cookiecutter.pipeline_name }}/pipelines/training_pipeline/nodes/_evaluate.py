import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from {{cookiecutter.pipeline_name}}.models import *
from ._cross_validation import TSGridSearchCV, ts_cv_split
from typing import List, Tuple

def evaluate_metrics(
    training_results: pd.DataFrame, 
    criterion: str = 'rmse') -> Tuple: 

    training_results = training_results[(training_results.training_status.isin(['success']))]
    training_results.replace([np.inf, -np.inf], np.nan, inplace=True)
    training_results['preds'] = training_results.preds.fillna(value=0)

    fold_metrics = training_results.groupby(['fold', 'estimator', 'agrup'], as_index=False, sort=False).apply(metrics)
    cv_metrics = fold_metrics.groupby(['estimator', 'agrup'], as_index=False, sort=False).agg(rmse=('rmse', np.nanmean), mape=('mape', np.nanmean))
    cv_metrics.sort_values(['agrup', criterion], inplace=True)
    
    best_estimators = cv_metrics.groupby(['agrup'], as_index=False).first()
    estimators_params = training_results.iloc[:, np.r_[0,8, 10:(len(training_results.columns))]].drop_duplicates(subset=['agrup', 'estimator'])
    best_estimators = pd.merge(best_estimators, estimators_params, how='left', on=['agrup', 'estimator'])
    
    training_results_be = pd.merge(training_results, 
                               best_estimators[['agrup', 'estimator']].drop_duplicates(),
                               how='inner',
                               on=['agrup', 'estimator'],
                               validate='m:1')
    acuracia = calc_acuracia(training_results_be, preds=[2]).drop(columns=["n_pred"])
    return best_estimators, acuracia

def calc_acuracia(
    df: pd.DataFrame, 
    preds: List = [2]) -> pd.DataFrame:

    sub1 = df[df.y_test.notnull()]
    sub1['carteira_liq'] = sub1.y_test*sub1.custo_unitario
    sub1['receita_proj'] = sub1.preds*sub1.custo_unitario
    sub1['delta'] = np.abs(sub1.carteira_liq - sub1.receita_proj)
    
    sub2 = sub1.groupby(['period', 'group_seg', 'n_pred'], as_index=False).agg(tt_receita_proj=('receita_proj', 'sum'),
                                                                             tt_carteira_liq=('carteira_liq', 'sum'),
                                                                             tt_delta=('delta', 'sum'))
    
    sub2['acuracia'] = 1 - (sub2.tt_delta/sub2.tt_receita_proj)
    sub2['acuracia'] = np.where(sub2.acuracia < 0, 0, sub2.acuracia)
    
    output = sub2.groupby(['group_seg', 'n_pred'], as_index=False).agg(acuracia=('acuracia', np.nanmean))
    output.sort_values(['n_pred', 'group_seg'], inplace=True)
    
    return output[output.n_pred.isin(preds)]

def test_engine(
    training_file: pd.DataFrame, 
    best_estimators: pd.DataFrame, 
    stride: int = 1, 
    frst: int = 2) -> Tuple[pd.DataFrame]:
    
    training_file.reset_index(inplace=True)
    forecast_file = pd.merge(training_file, best_estimators, how='inner', on='agrup', validate='m:1')
    forecast_file["periodo"] = pd.to_datetime(forecast_file["periodo"]) 
    final_results = pd.DataFrame()
    test_results = pd.DataFrame()
    for g, g_data in forecast_file.groupby(['agrup'], sort=False):
        group_seg = g_data.group_seg.iloc[0]
        data = g_data.set_index('periodo').copy()
        ts = data.volume_carteira
        estimator = eval(g_data.estimator.iloc[0])
        cv_split = ts_cv_split(ts, stride=stride, frst=frst)
        test_dict = {}
        df_model = test_model(ts, 
                               cv_split, 
                               estimator)

        test_dict["best_model"] = df_model
        df_test = pd.concat((df for _, df in test_dict.items()), ignore_index=True)

        compl_data = g_data[['agrup', 'group_seg', 'custo_unitario']].reset_index(drop=True).head(1)
        rep_compl_data = pd.concat([compl_data]*df_test.shape[0], ignore_index=True)

        df_test = pd.concat((rep_compl_data, df_test), axis=1)
        df_test['agrup'] = g
        test_results = pd.concat((test_results, df_test), ignore_index=True)

    
    acuracia = calc_acuracia(test_results, preds=[2]).drop(columns=["n_pred"])
    return test_results, acuracia

def test_model(y, cv_split, model):
    
    
    
    cv = TSGridSearchCV(estimator=model,
                                      params_grid=[model.get_params()],
                                      cv_split=cv_split,
                                      fit_error='warn',
                                      n_jobs=-1,
                                       verbose=0)
    
    test_preds = cv(y, X=None)
    
    df_test = pd.concat((pd.DataFrame(test_preds[i]) for i in range(len(test_preds))), ignore_index=True)
    
    return df_test

def measure_rmse(actual, predicted):
    return np.sqrt(mean_squared_error(actual, predicted))

def measure_mape(actual, predicted):
    return np.nanmean(np.abs((actual - predicted) / actual)*100)


def metrics(x):
    d = {}
    d['rmse'] = measure_rmse(x['y_test'], x['preds'])
    d['mape'] = measure_mape(x['y_test'], x['preds'])

    return pd.Series(d, index=['rmse', 'mape'])
