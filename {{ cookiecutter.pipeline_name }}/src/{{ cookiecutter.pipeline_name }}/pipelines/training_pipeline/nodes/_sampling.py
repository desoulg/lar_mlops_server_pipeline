import pandas as pd
import numpy as np

def sampling_training_file(
    training_file: pd.DataFrame, 
    n: int = 1):
    groups = training_file.group_seg.unique()
    sampling = pd.DataFrame()
    for group in groups:
        df = training_file[training_file.group_seg == group]
        agrups = np.random.choice(df.agrup.unique(), size=n, replace=False)
        sampling = sampling.append(df, ignore_index=True)                