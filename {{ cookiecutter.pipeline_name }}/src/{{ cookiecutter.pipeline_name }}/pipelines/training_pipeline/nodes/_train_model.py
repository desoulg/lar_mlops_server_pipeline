from {{cookiecutter.pipeline_name}}.utils import model_from_string
from typing import Dict, Optional, Union, List, Any
from ._cross_validation import TSGridSearchCV, ts_cv_split
from ._params_search import build_params_search
from pmdarima.model_selection import RollingForecastCV, SlidingWindowForecastCV
import pandas as pd
import logging
import time 
import numpy as np
from tqdm import tqdm
import warnings
import pandas as pd

warnings.filterwarnings("ignore")
logger = logging.getLogger(__name__)

def filter_data(
    training_file: pd.DataFrame, 
    split: Union[int, str],
    split_info: Dict):
    if split_info["level"] == "agrup":
        splits = np.array_split(training_file["agrup"].unique(), split_info["n_splits"])
        if len(splits) > split:
            return training_file[training_file["agrup"].isin(splits[split])]
    elif split_info["level"] == "group_seg":
        return training_file[training_file["group_seg"] == split]
    return pd.DataFrame()


def train_engine(
    training_file: pd.DataFrame, 
    split: Union[int, str],
    split_info: Dict,
    model: Dict, 
    stride: int = 1, 
    frst: int = 12, 
    pct_train: float = 0.7,
    ):
    training_file = filter_data(training_file, split, split_info)
    training_results = pd.DataFrame()
    if training_file.shape[0]:
        logger.info(f"Group:{split} Size:{training_file.agrup.nunique()}")
        # Iter over group series
        for serie_id in tqdm(training_file.agrup.unique(), total=training_file.agrup.nunique()):
            serie_results = train_serie_engine(training_file, model, stride, frst, serie_id=serie_id)
            if serie_results is not None:
                training_results = pd.concat((training_results, serie_results), ignore_index=True)
        if training_results.shape[0]:
            trained_time_series = len(training_results.agrup.unique()) 
        else:
            trained_time_series = 0 
        logger.info(f"# of trained Time Series: {trained_time_series}")
        if trained_time_series:
            failure_rate = ((training_results.training_status == "failed").sum()/training_results.shape[0])*100
            logger.info(f"Failure rate: {failure_rate}%")
        
    return training_results
      

def train_serie_engine(
    training_file: pd.DataFrame, 
    model: Dict, 
    stride: int = 1, 
    frst: int = 12, 
    pct_train: float = 0.7,
    serie_id: Optional[str] = None) -> pd.DataFrame:
    """
    This node train a model for a series from the training file. For each combination of model parameters, 
    cross-validation will be performed that will be used to identify the best model.
        Args:
            training_file: DataFrame containing all series.
            model: Dictionary containing model info and parameters grid.
            stride: The size of step taken to increase the training sample size.
            frst: The size of forecast.
            serie_id: Série id.
        Returns:
            pd.DataFrame: Model Predictions in Grid Search CV
    """
    
    training_results = pd.DataFrame()
    
    if training_file.periodo.dtype == "object":
        training_file["periodo"] = pd.to_datetime(training_file.periodo)
    
    # Get series data
    
    g_data  =  training_file[training_file.agrup == serie_id] 
    if g_data.shape[0] > 0:
        group_seg = g_data.group_seg.iloc[0]
    else:
        return pd.DataFrame()
    
    
    training_results = pd.DataFrame()
    data = g_data.set_index('periodo').copy()
    ts = data.volume_carteira
    
    # Cross Validation Split
    cv_split = ts_cv_split(ts, stride=stride, frst=frst, pct_train=0.7)
    
    
    model_groups = list(model['params'].keys())
    models = model['params']
    model_name = model['model_name']
    if group_seg in model_groups:
        model_group = group_seg   
    elif "all" in model_groups:
        model_group = "all"
    else:
        return None
    # If model is in serie model group
    #logger.info(f"Execute model {model_name} using cross validation with stride {stride} and frst {frst}")
    model_results = train_model(ts, 
                           cv_split, 
                           model['model_class'], 
                           model['default_args'],
                           models[model_group]['params_search'])

    compl_data = g_data[['agrup','level', 'group_seg', 'custo_unitario']].reset_index(drop=True).head(1)
    rep_compl_data = pd.concat([compl_data]*model_results.shape[0], ignore_index=True)
    model_results = pd.concat((rep_compl_data, model_results), axis=1)

    model_results['agrup'] = serie_id
    training_results = pd.concat((training_results, model_results), ignore_index=True)
    

    
    return training_results
    
def train_model(
    y: Union[pd.Series, np.array], 
    cv_split: Union[RollingForecastCV, SlidingWindowForecastCV], 
    model_class: str, 
    model_default_args: Dict, 
    params: Dict):
    """
    This function train a model in a serie using cross validation with grid search.
        Args:
            y: Serie
            cv_split: Indices to each fold of cross validation
            model_class: String of model class
            model_default_args: Default args to initialize model
            params: Parameters Grid of model
        Returns:
            pd.DataFrame: Model Predictions in Grid Search CV
    """
    model_params = build_params_search(params)
    
    estimator = model_from_string(model_class, model_default_args)
    
    grid_search_cv = TSGridSearchCV(estimator=estimator,
                                      params_grid=model_params,
                                      cv_split=cv_split,
                                      fit_error='warn',
                                      n_jobs=-1,
                                       verbose=0)
    
    training = grid_search_cv(y, X=None)
    
    df_training = pd.concat((pd.DataFrame(training[i]) for i in range(len(training))), ignore_index=True)
    
    return df_training

def join_results(*dfs):
    """
    This node concatenate DataFrames
        Args:
            dfs: List of DataFrames
    """
    results = list(filter(lambda x: x.shape[0] > 0, dfs))
    logger.info(f"# of trained Time Series: {len(results)}")
    if len(results) == 0:
        return pd.DataFrame()
    return pd.concat(results)