import numpy as np
import pandas as pd
from typing import Union, Tuple, List
from pmdarima import model_selection
from sklearn import base
from itertools import product
from joblib import Parallel, delayed, cpu_count
from sklearn.metrics import mean_squared_error
import time
import warnings
warnings.filterwarnings("ignore")
import logging

logger = logging.getLogger(__name__)
def ts_cv_split(
    ts: Union[pd.Series, np.array], 
    method: str = 'Rolling', 
    stride: int = 1, 
    frst: int =12, 
    pct_train: float = 0.7, 
    window_size: int = 24) -> Union[model_selection.RollingForecastCV, model_selection.SlidingWindowForecastCV]:

    
    if method == 'Rolling':
        
        initial = int(len(ts)*pct_train)
        n_trainings = len(ts) - (initial + frst)

        while (n_trainings < 3) & (frst != 1):
            frst -= 1
            n_trainings = len(ts) - (initial + frst)
            
        cv = model_selection.RollingForecastCV(step=stride, h=frst, initial=initial)
    
    elif method == 'SlidingWindow':
        cv = model_selection.SlidingWindowForecastCV(window_size=window_size, step=stride, h=frst)
        
    else: 
        raise NotImplementedError("The method you are trying to use is not implemented.")
    #logger.info(f"CV -> ts_size:{ts.shape[0]} initial:{initial} n_trainings:{n_trainings}")
    return cv

def _safe_indexing(
    X: Union[pd.Series, pd.DataFrame, np.array], 
    indices: Union[List[int], np.array]) -> Union[pd.Series, pd.DataFrame, np.array]:
    """Slice an array or dataframe."""
    
    # slicing dataframe
    if hasattr(X, 'iloc'):
        return X.iloc[indices]
    
    # slicing 2D array
    if hasattr(X, 'ndim') and X.ndim == 2:
        return X[indices, :]
    
    # list or 1d array
    return X[indices]

def _safe_split(
    y: Union[pd.Series, np.array], 
    X: Union[pd.Series, pd.DataFrame, np.array, None], 
    train: Union[List[int], np.array], 
    test: Union[List[int], np.array]) -> Tuple:
    """Performs the CV indexing given the indices"""
    
    y_train, y_test = _safe_indexing(y, train), _safe_indexing(y, test)
    
    if X is None:
        X_train = X_test = None
    else:
        X_train, X_test = _safe_indexing(X, train), _safe_indexing(X, test)
        
    return y_train, y_test, X_train, X_test

def _fit_and_pred(
    fold, 
    estimator, 
    y, 
    X, 
    parameters, 
    train, 
    test, 
    verbose, 
    fit_error):
    """Fit estimator and compute scores for a given dataset split."""
    
    msg = 'fold=%i' % fold
    if verbose > 1:
        print("[CV] %s %s" % (msg, (64 - len(msg)) * '.'))

    start_time = time.time()
    y_train, y_test, X_train, X_test = _safe_split(y, X, train, test)
    
    if parameters is not None:
        # clone after setting parameters in case any parameters
        # are estimators (like pipeline steps)
        # because pipeline doesn't clone steps in fit
        cloned_parameters = {}
        for k, v in parameters.items():
            cloned_parameters[k] = base.clone(v, safe=False)
        
        estimator = estimator.set_params(**cloned_parameters)

    try:
        
        estimator.fit(y_train, X=X_train) 
        fit_time = time.time() - start_time

        start_time = time.time()
        preds = estimator.predict(len(test), X=X_test)
        preds[preds < 0] = 0
        metric = mean_squared_error(test, preds)
        preds = np.round(preds, 0)
        
        if not np.isnan(preds).all():
            status = 'success'
        else:
            status = 'failed'
        message = None
        pred_time = time.time() - start_time

    except Exception as e:
        fit_time = time.time() - start_time
        start_time = time.time()
        preds = np.empty(len(test))
        preds[:] = np.nan
        status = 'failed'
        message = str(e)
        pred_time = time.time() - start_time
        metric = np.nan
        if fit_error == 'raise':
            raise
            
        elif fit_error == 'warn':
            warnings.warn("Estimator fit failed.")
            
        elif fit_error == 'ignore':
            pass

    if verbose > 2:
        total_time = pred_time + fit_time
        msg += ", [time=%.3f sec]" % (total_time)
        print(msg)  
    ret = {'period': y_test.index,
           'preds': preds,
           'y_test': y_test.values,
           'fold': np.tile([fold], preds.shape[0]),
           'n_pred': np.arange(1, preds.shape[0]+1),
           'estimator': np.tile([str(estimator)], preds.shape[0]),
           'training_status': np.tile([status], preds.shape[0]),
           'error_message': np.tile([message], preds.shape[0]),
           'metric': metric,
          }
    
    for param, v in cloned_parameters.items():
        ret["param_"+param] = np.tile(str(v), preds.shape[0])
        
    return ret

class TSGridSearchCV():
    
    def __init__(self, estimator, params_grid, cv_split, fit_error='warn', n_jobs=-1, verbose=1):
        
        self.estimator = estimator
        self.params_grid = params_grid
        self.cv_split = cv_split
        self.fit_error = fit_error
        self.verbose = verbose
        
        if n_jobs == -1:
            self.n_jobs = cpu_count()
        else:
            self.n_jobs = max(min(n_jobs, cpu_count), 1)
    
    
    def __call__(self, y, X=None):
        
        if isinstance(self.params_grid, dict):
            params = [dict(zip(self.params_grid, t)) for t in product(*self.params_grid.values())] 
        else: 
            params = self.params_grid
                
        n_folds = len(list(self.cv_split._iter_train_test_indices(y, X)))
        n_params =  len(params)
        n_fits = n_folds * n_params
        
        if self.verbose > 0:
            print(f"Fitting {n_folds} folds for each of {n_params} candidates, totalling {n_fits} fits")
        
        parallel = Parallel(n_jobs=self.n_jobs, pre_dispatch="2*n_jobs", verbose=self.verbose)
        
        out = parallel(delayed(_fit_and_pred)(fold = fold,
                                              estimator = base.clone(self.estimator),
                                              y = y,
                                              X = X,
                                              parameters = parameters,
                                              train = train,
                                              test = test,
                                              verbose = self.verbose,
                                              fit_error = self.fit_error
                                              )
                                              for (cand_idx, parameters), (fold, (train, test)) in product(
                                                  enumerate(params), enumerate(self.cv_split.split(y, X)))
                      )
        models = map(lambda x: x["estimator"][0], out)
        metrics = map(lambda x: x["metric"], out)
        results = pd.DataFrame({"estimator": models, "metric": metrics})
        best_estimator = results.groupby("estimator").mean().idxmin()["metric"]
        out = list(filter(lambda x: x["estimator"][0] == best_estimator, out))
        return out