from functools import reduce
from operator import add

from kedro.pipeline import Pipeline, node
from {{cookiecutter.pipeline_name}}.pipelines import big_query as bq
from {{cookiecutter.pipeline_name}}.pipelines import preprocess_pipeline as pp
from {{cookiecutter.pipeline_name}}.utils import clean_name

from kedro.pipeline.modular_pipeline import pipeline
from kedro.framework.session import get_current_session
from .nodes import  join_results, train_engine

import logging

logger = logging.getLogger(__name__)


def create_modeling_pipelines(configs) -> Pipeline:
    def _pipeline(name):
        return Pipeline([
            node(
                func=train_engine,
                inputs=["training_data",
                        "split_id:id",
                        "params:split_info",
                        "params:model",
                        "params:stride",
                        "params:frst",
                        "params:pct_train"],
                outputs="training_results",
                name=name
            ),
        ])

    combined_pipelines = reduce(add, [pipeline(
            pipe=_pipeline(name=clean_name(f"training_{split_id}_{level}_{model}")),
            parameters={
                "params:model": f"params:models.{level}.{model}",
                "split_id:id": f"split_id:{split_id}"
            },
            inputs={"training_data": f"training_data_{level}"},
            outputs={
                "training_results": clean_name(f"training_results_{split_id}_{level}_{model}")
            },
        )
        for split_id, level, model in configs
    ])

    return pipeline(pipe=combined_pipelines)


def create_post_processing_pipeline(configs, levels):
    return Pipeline([
        node(
            func=join_results,
            inputs=[clean_name(f"training_results_{split_id}_{level}_{model}") for split_id, level, model in
                    filter(lambda x: x[1] == level, configs)],
            outputs=f"training_results_{level}",
            name=f"results_{level}"
        )
        for level in levels
    ])


def pipelines_configs():
    session = get_current_session()
    context = session.load_context()
    catalog = context.catalog
    levels = catalog.load("params:levels")
    models = catalog.load("params:models")
    split_info = catalog.load("params:split_info")
    
    configs = []
    for level in levels:
        if split_info["level"] == "agrup":
            for i in range(split_info["n_splits"]):
                for model in models[level]:
                    configs.append((i, level, model))
    logger.info(f"# of configs:{len(configs)}")
    return configs, levels, split_info


def create_pipeline() -> Pipeline:
    configs, levels, split_info = pipelines_configs()

    modeling_pipelines = create_modeling_pipelines(configs)
    post_processing_pipeline = create_post_processing_pipeline(configs, levels)

    return modeling_pipelines + post_processing_pipeline