import numpy as np
import pandas as pd
from typing import List
from sklearn.metrics import mean_squared_error
import logging

logger = logging.getLogger(__name__)
def evaluate_metrics(
    training_results: pd.DataFrame, 
    criterion: str = 'rmse') -> pd.DataFrame: 
    """
    This node evaluate training predictions
    Args:
        training_results: Training predictions.
        criterion: Metric used to evaluate.
    Returns:
        pd.DataFrame -> Best estimators to each serie.
        pd.DataFrame -> Metrics results
    """
    training_results = training_results[(training_results.training_status.isin(['success']))]
    training_results.replace([np.inf, -np.inf], np.nan, inplace=True)
    training_results['preds'] = training_results.preds.fillna(value=0)

    fold_metrics = training_results.groupby(['fold', 'estimator', 'agrup'], as_index=False, sort=False).apply(metrics)
    cv_metrics = fold_metrics.groupby(['estimator', 'agrup'], as_index=False, sort=False).agg(rmse=('rmse', np.nanmean), mape=('mape', np.nanmean))
    cv_metrics.sort_values(['agrup', criterion], inplace=True)
    'level', 'group_seg', 'custo_unitario', 'period', 'preds','y_test', 'fold', 'n_pred'
    best_estimators = cv_metrics.groupby(['agrup'], as_index=False).first()
    serie_columns = ['level', 'group_seg', 'custo_unitario', 'period', 'preds','y_test', 'fold', 'n_pred']
    estimators_params = training_results.drop(columns=serie_columns).drop_duplicates(subset=['agrup', 'estimator'])
    best_estimators = pd.merge(best_estimators, estimators_params, how='left', on=['agrup', 'estimator'])
    
    training_results_be = pd.merge(training_results, 
                               best_estimators[['agrup', 'estimator']].drop_duplicates(),
                               how='inner',
                               on=['agrup', 'estimator'],
                               validate='m:1')
    acuracia = calc_acuracia(training_results_be, preds=[2]).drop(columns=["n_pred"])
    return best_estimators, acuracia
  
def calc_acuracia(
    df: pd.DataFrame, 
    preds: List[str] = [2]) -> pd.DataFrame:
    """
    This function calculate accuracy in training predictions.
        Args:
            df: DataFrame with predictions.
            preds: Forecast step which will be used in the accuracy calculation.
        Returns:
            pd.DataFrame -> Accuracy results.
    """
    sub1 = df[df.y_test.notnull()]
    sub1['carteira_liq'] = sub1.y_test*sub1.custo_unitario
    sub1['receita_proj'] = sub1.preds*sub1.custo_unitario
    sub1['delta'] = np.abs(sub1.carteira_liq - sub1.receita_proj)
    
    sub2 = sub1.groupby(['period', 'group_seg', 'n_pred'], as_index=False).agg(tt_receita_proj=('receita_proj', 'sum'),
                                                                             tt_carteira_liq=('carteira_liq', 'sum'),
                                                                             tt_delta=('delta', 'sum'))
    
    sub2['acuracia'] = 1 - (sub2.tt_delta/sub2.tt_receita_proj)
    sub2['acuracia'] = np.where(sub2.acuracia < 0, 0, sub2.acuracia)
    
    output = sub2.groupby(['group_seg', 'n_pred'], as_index=False).agg(acuracia=('acuracia', np.nanmean))
    output.sort_values(['n_pred', 'group_seg'], inplace=True)
    
    return output[output.n_pred.isin(preds)]

def measure_rmse(actual, predicted):
    return np.sqrt(mean_squared_error(actual, predicted))

def measure_mape(actual, predicted):
    return np.nanmean(np.abs((actual - predicted) / actual)*100)


def metrics(x):
    d = {}
    d['rmse'] = measure_rmse(x['y_test'], x['preds'])
    d['mape'] = measure_mape(x['y_test'], x['preds'])

    return pd.Series(d, index=['rmse', 'mape'])

