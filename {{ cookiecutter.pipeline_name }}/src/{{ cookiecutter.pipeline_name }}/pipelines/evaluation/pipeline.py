from functools import reduce
from operator import add
from typing import List

from kedro.pipeline import Pipeline, node
from kedro.pipeline.modular_pipeline import pipeline
from kedro.framework.session import get_current_session

from .nodes import evaluate_metrics

def create_evaluation_pipelines(levels) -> Pipeline:
    def _pipeline(level):
        return Pipeline(
            [
                node(
                    func=evaluate_metrics,
                    inputs=["training_results", "params:metric"],
                    outputs=["best_estimators", "acuracia"],
                    name=f"evaluate_metrics_{level}"
                ),
            ]
        )

    # Combine modeling pipeliens into one pipeline object
    combined_pipelines = reduce(add, [
        pipeline(
            pipe=_pipeline(level),
            parameters={
                "params:metric": "params:metric"
            },
            inputs={
                "training_results": f"training_results_{level}"
            },
            outputs={
                "best_estimators": f"best_estimators_{level}",
                "acuracia": f"acuracia_{level}",
            }
        )
        for level in levels
    ])

    # Namespace consolidated modeling pipelines
    return pipeline(pipe=combined_pipelines)


def create_pipeline() -> Pipeline:
    try:
        session = get_current_session()
        context = session.load_context()
        catalog = context.catalog

        levels = catalog.load("params:levels")
   
    except:
        levels = ['level0', 'level1']

    # Instantiate a new modeling pipeline for every model type

    return create_evaluation_pipelines(levels)