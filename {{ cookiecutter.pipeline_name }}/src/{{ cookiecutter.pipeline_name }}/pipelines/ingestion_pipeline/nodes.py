# Copyright 2021 QuantumBlack Visual Analytics Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
# NONINFRINGEMENT. IN NO EVENT WILL THE LICENSOR OR OTHER CONTRIBUTORS
# BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF, OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# The QuantumBlack Visual Analytics Limited ("QuantumBlack") name and logo
# (either separately or in combination, "QuantumBlack Trademarks") are
# trademarks of QuantumBlack. The License does not grant you any right or
# license to the QuantumBlack Trademarks. You may not use the QuantumBlack
# Trademarks or any confusingly similar mark as a trademark for your product,
# or use the QuantumBlack Trademarks in any other manner that might cause
# confusion in the marketplace, including but not limited to in advertising,
# on websites, or on software.
#
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This is a boilerplate pipeline 'ingestion_pipeline'
generated using Kedro 0.17.5
"""

from typing import List

import numpy as np
import pandas as pd
from datetime import date, datetime
from functools import reduce
from operator import add

def get_datatype_from_string(string: str):
    attr = getattr(np, string, False)
    if not attr:
        return string
    return attr


def get_month(dat, inc):
    return (dat.month + inc - 1) % 12 + 1


def get_year(dat, inc):
    return dat.year + (dat.month + inc - 1) // 12
    
    
def string_to_date(date):
    if isinstance(date, str):
        return datetime.strptime(date, "%Y-%m-%d").date()
    else:
        return date
    
    
def test_schema(abt: pd.DataFrame, input_columns: List[str]):
    # check if columns are present in master table
    columns_test = [k for k, v in input_columns.items() if abt[k].dtype != v]

    # if columns are missing, raise error
    if columns_test:
        raise KeyError('Invalid master table. Schema mismatch:', columns_test)
    else:
        print('All required columns were found.')


def test_conversion_types(abt: pd.DataFrame, check_conversion_dtypes: List[str]):
    for k, v in check_conversion_dtypes.items():
        if v == "int64":
            abt[k] = abt[k].fillna(0)
        abt[k].astype(get_datatype_from_string(v))


def test_timeseries_size(series_records: pd.DataFrame):
    # get unique values from series_records
    series_len = series_records.s_size.unique()
    
    # if all series are the same length then OK, else invalid
    if len(series_len) == 1:
        print('ID length: {}'.format(series_len[0]))
    else:
        raise ValueError('Invalid master table. All series must be the same size.')


def test_invalid_timeseries(series_records: pd.DataFrame):
    # get empty series
    empty_series = series_records[series_records.s_count == 0]
        
    # check for empty series
    if empty_series.empty:
        print('All series have at least 1 point.')
    else:
        raise ValueError('Invalid master table. Empty series were found.')


def test_min_max_dates(series_records: pd.DataFrame):
    min_dates = series_records[series_records.s_min == series_records.s_min.min()]
    max_dates = series_records[series_records.s_max == series_records.s_max.max()]
    
    if len(min_dates) == len(series_records) and len(max_dates) == len(series_records):
        print("All series have the same start and end date")
    else:
        raise ValueError('Invalid master table. Timeseries does not have the same start and end date.')

        
def test_updated_base(series_records: pd.DataFrame, base_update_day: int):
    max_date = string_to_date(series_records.s_max.max())
    today = date.today()    
    
    if today.day >= base_update_day and max_date.month == get_month(today, -1) and max_date.year == get_year(today, -1):
        print("Master table is updated")
    elif today.day <= base_update_day and max_date.month == get_month(today, -2) and max_date.year == get_year(today, -2):
        print("Master table is updated")
    else:
        raise ValueError("Master Table is not updated")

        
def test_unique_agrup(abt: pd.DataFrame, agrup: List[str]):
    groupby_list = agrup + ["periodo"]
    
    unique_df = abt.copy()
    unique_df = reduce(add, [unique_df[i].astype(str) for i in groupby_list])
    
    if len(pd.unique(unique_df)) == len(abt):
        print("Master table has unique agrups")
    else:
        raise ValueError("Master table has agrups duplicated")

        
def test_master_table(abt: pd.DataFrame,
                      agrup: List[str],
                      input_columns: List[str],
                      check_conversion_dtypes: List[str],
                      base_update_day: int) -> pd.DataFrame:
    
    # count series records
    series_records = abt.groupby(agrup,
                                 dropna=False,
                                 as_index=False,
                                 sort=False)\
                        .agg(s_size=('material', 'size'),
                             s_count=('volume_carteira', 'count'),
                             s_min=("periodo", "min"),
                             s_max=("periodo", "max"))
    
    test_schema(abt, input_columns)
    test_conversion_types(abt, check_conversion_dtypes)
    test_timeseries_size(series_records)
    test_invalid_timeseries(series_records)
    test_min_max_dates(series_records)
    test_updated_base(series_records, base_update_day)
    test_unique_agrup(abt, agrup)
    
    return abt