from typing import Dict


from kedro.pipeline import node, Pipeline
from {{cookiecutter.pipeline_name}}.pipelines.big_query.nodes import (
    log_running_time,
    make_query,
)

def create_pipeline(**kwargs) -> Dict[str, Pipeline]:
    return Pipeline(
        [
            node(
                func=make_query,
                inputs="abt_gcp",
                outputs="abt",
                name="query_abt",
                tags=["{{cookiecutter.pipeline_name}}","abt"]
            ),
            node(
                func=make_query,
                inputs="abt_client_gcp",
                outputs="abt_client",
                name="query_abt_client",
                tags=["{{cookiecutter.pipeline_name}}","abt_client"]
            ),
        ],
        tags=["bq_tag"],
    )