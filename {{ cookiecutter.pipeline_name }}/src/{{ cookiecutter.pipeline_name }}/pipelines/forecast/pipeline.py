from functools import reduce
from operator import add
from typing import List

from kedro.pipeline import Pipeline, node
from kedro.pipeline.modular_pipeline import pipeline
from kedro.framework.session import get_current_session

from .nodes import forecasting, join_results, client_forecast
import logging

logger = logging.getLogger(__name__)

def create_forecasting_pipelines(configs) -> Pipeline:
    def _pipeline(level, split_id):
        return Pipeline(
            [
                node(
                    func=forecasting,
                    inputs=["training_data",
                            "split_id:id",
                            "params:split_info",
                            "best_estimators",
                            "params:forecast_size",
                            "params:training_start_date",
                            "params:imr_factor",
                            "params:mr_range",
                            "params:auto_arima"],
                    outputs="final_results",
                    name=f"forecast_{level}_{split_id}"
                ),
                node(
                    func=client_forecast,
                    inputs=["abt_client",
                            "training_data",
                            "final_results",
                            "params:agrup",
                            "split_id:id",
                            "params:split_info",
                            "params:client_hist_size",
                            "params:forecast_client_method",
                            "params:client_model_args"],
                    outputs="clients_forecast",
                    name=f"clients_forecast_{level}_{split_id}"
                ),
            ]
        )

    combined_pipelines = reduce(add, [
        pipeline(
            pipe=_pipeline(level, split_id),
            inputs={
                "training_data": f"training_data_{level}",

                "split_id:id": f"split_id:{split_id}",
                "best_estimators": f"best_estimators_{level}"
            },
            outputs={
                "final_results": f"final_results_{split_id}_{level}",
                "clients_forecast": f"clients_forecast_{split_id}_{level}",
            },
            parameters={
                "params:agrup": f"params:levels.{level}.agrup",
            }

        )
        for split_id, level in configs
    ])

    return pipeline(pipe=combined_pipelines)


def create_join_forecast_pipeline(configs, levels):
    return Pipeline([
        node(
            func=join_results,
            inputs=[f"final_results_{split_id}_{level}" for split_id, level in
                    filter(lambda x: x[-1] == level, configs)],
            outputs=f"final_results_{level}",
            name=f"final_results_{level}"
        )
        for level in levels
    ])


def create_join_clients_pipeline(configs, levels):
    return Pipeline(
        [
            node(
                func=join_results,
                inputs=[f"clients_forecast_{split_id}_{level}" for split_id, level in
                        filter(lambda x: x[-1] == level, configs)],
                outputs=f"final_results_clients_{level}",
                name=f"clients_forecast_{level}"
            )
            for level in levels
        ]
    )


def pipelines_configs():
    session = get_current_session()
    context = session.load_context()
    catalog = context.catalog
    levels = catalog.load("params:levels")
    split_info = catalog.load("params:split_info")
    
    configs = []
    for level in levels:
        if split_info["level"] == "agrup":
            for i in range(split_info["n_splits"]):
                configs.append((i, level))
    logger.info(f"# of forecast configs:{len(configs)}")
    return configs, levels


def create_pipeline() -> Pipeline:
    try:
        session = get_current_session()
        context = session.load_context()
        catalog = context.catalog

        levels = catalog.load("params:levels")
    except:
        levels = ['level0', 'level1']

    configs, levels = pipelines_configs()

    forecasting_pipelines = create_forecasting_pipelines(configs)
    join_forecast_pipeline = create_join_forecast_pipeline(configs, levels)
    join_clients_pipeline = create_join_clients_pipeline(configs, levels)

    return forecasting_pipelines + join_forecast_pipeline + join_clients_pipeline