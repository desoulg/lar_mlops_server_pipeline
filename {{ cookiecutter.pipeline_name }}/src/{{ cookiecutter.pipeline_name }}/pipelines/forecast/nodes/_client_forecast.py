from {{cookiecutter.pipeline_name}}.models import ExponentialSmoothing
from sklearn.metrics import mean_squared_error
import numpy as np
import pandas as pd
from tqdm import tqdm
import time
import pandas as pd
from ._utils import filter_data
from joblib import Parallel, delayed, cpu_count

def normalize(x):
    x[x<0] = 0
    return x / x.sum()

def normalize_bos(data, proj_target="bos"):
    data["bos"] = normalize(data[proj_target].values)
    data["bos"] = data["bos"].fillna(0)
    return data

def calc_bos_proj(agrup, emissorord, serie_data, proj_target, forecast_periods, seasonal_periods, seasonal, trend):
    serie_data["bos"] = 0
    nonzeros = serie_data["volume_carteira_total"] > 0
    serie_data.loc[nonzeros, "bos"] = serie_data.loc[nonzeros, "volume_carteira_client"]/serie_data.loc[nonzeros, "volume_carteira_total"]
    ts = serie_data.set_index("periodo")[proj_target].astype('float64')
    try:
        estimator = ExponentialSmoothing(seasonal_periods=seasonal_periods, seasonal=seasonal, trend=trend)
        estimator.fit(ts)
    except:
        estimator = ExponentialSmoothing()
        estimator.fit(ts)
    forecast = estimator.predict(forecast_periods.shape[0])
    bos_client = pd.DataFrame()
    bos_client[proj_target] = forecast.values
    bos_client[proj_target] = bos_client[proj_target].fillna(0)
    bos_client["emissorord"] = emissorord
    bos_client["periodo"] = forecast_periods
    bos_client["agrup"] = agrup
    return bos_client

def bos_proj(
    final_results, 
    client, 
    forecast_column="forecast", 
    proj_target="bos", 
    seasonal_periods=3,
    seasonal=None, 
    trend=None):
    
    bos_clients = pd.DataFrame()
    forecast_periods = final_results.periodo.unique()
    n_series = client[["agrup", "emissorord"]].drop_duplicates().shape[0]
    parallel = Parallel(n_jobs=min(cpu_count(), n_series), pre_dispatch="2*n_jobs", verbose=2)
    bos = parallel(delayed(calc_bos_proj)(agrup, 
                                         emissorord, 
                                         serie_data,
                                         proj_target, forecast_periods,
                                         seasonal_periods, seasonal, trend)
                     for (agrup, emissorord), serie_data in client.groupby(["agrup", "emissorord"]))
    bos_clients = pd.concat(bos, ignore_index=True)
    forecast_clients = pd.merge(final_results, bos_clients, on=["agrup", "periodo"])
    forecast_clients = forecast_clients.groupby(["agrup","periodo"]).apply(lambda data: normalize_bos(data, proj_target=proj_target))
    forecast_clients["forecast_client"] = forecast_clients[forecast_column] * forecast_clients["bos"]
    return forecast_clients

def mean_bos(final_results, client, forecast_column="forecast"):
    # Historical BOS
    historical = client.groupby(["agrup", "emissorord"]).mean().reset_index()
    historical["bos"] = 0
    nonzeros = historical["volume_carteira_total"] > 0
    historical.loc[nonzeros, "bos"] = historical.loc[nonzeros, "volume_carteira_client"]/historical.loc[nonzeros, "volume_carteira_total"]
    
    # Calculation of the forecast of each client from DOS*Forecast Level 1
    forecast_clients = pd.merge(final_results, historical[["agrup", "emissorord", "bos"]], on="agrup")
    forecast_clients = forecast_clients.groupby(["agrup","periodo"]).apply(lambda data: normalize_bos(data, proj_target="bos"))
    forecast_clients["forecast_client"] = forecast_clients[forecast_column]*forecast_clients["bos"]
    forecast_clients = forecast_clients[["agrup", 
                                         "periodo", 
                                         "emissorord", 
                                         "custo_unitario", 
                                         forecast_column, 
                                         "forecast_client", 
                                         "bos"]]
    
    return forecast_clients

def client_forecast(
    abt_client,
    training_data,
    final_results, 
    agrup,
    split,
    split_info,
    months=6,
    method="mean",
    model_args={}):
    
    training_data = filter_data(training_data, split, split_info)
    # Data filtering to keep only data from the last few months.
    now = training_data.periodo.max()
    last = now - pd.DateOffset(months=months)
    abt_client["periodo"] = pd.to_datetime(abt_client.periodo)
    
    filtered_abt_client = abt_client[abt_client.periodo >= last]
    filtered_training_data = training_data[training_data.periodo >= last]
    filtered_final_results = final_results[final_results.periodo > training_data.periodo.max()]
    
    # Agrup column 
    filtered_abt_client['agrup'] = list(map(str, zip(*[filtered_abt_client[c] for c in agrup])))
    filtered_abt_client = filtered_abt_client[filtered_abt_client.agrup.isin(filtered_training_data.agrup.unique())]
    
    # Merge -> ABT_client/ABT
    abt_columns = ["agrup", "periodo", "volume_carteira"]
    client_columns = ["agrup", "periodo", "emissorord", "volume_carteira"]
    client = pd.merge(filtered_abt_client[client_columns], 
                      filtered_training_data[abt_columns], 
                      on=["agrup", "periodo"], suffixes=('_client', '_total'))
    
    client["volume_carteira_client"] = client["volume_carteira_client"].fillna(0)
    client["volume_carteira_client"] = client["volume_carteira_client"].astype("float32")
    
    if method == "mean":
        forecast_clients = mean_bos(filtered_final_results, client,forecast_column="forecast")
    elif method == "proj":
        forecast_clients = bos_proj(filtered_final_results, client,forecast_column="forecast", proj_target="volume_carteira_client", **model_args)
        forecast_clients_mean = mean_bos(filtered_final_results, client,forecast_column="forecast")
        forecast_clients = pd.merge(forecast_clients, forecast_clients_mean, on=["agrup", "emissorord", "periodo"], suffixes=('_proj', '_mean'))
        forecast_clients = forecast_clients.groupby(["agrup", "periodo"]).apply(adjust_forecast)
    else:
        raise ValueError(f"The method {method} has not been implemented")
    forecast_clients["forecast_client"] = forecast_clients["forecast_client"].apply(np.round).astype("int32")
    return forecast_clients

def adjust_forecast(x):
    x["bos"] = x["bos_mean"] if x["bos_proj"].sum() == 0 else x["bos_proj"]
    x["forecast_client"] = x["forecast_proj"] * x["bos"]
    return x