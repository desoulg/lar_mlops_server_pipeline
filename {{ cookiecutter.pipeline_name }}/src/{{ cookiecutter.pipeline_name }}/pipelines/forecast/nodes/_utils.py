from typing import Dict, Union
import pandas as pd
import numpy as np
import logging
import warnings
from datetime import datetime

warnings.filterwarnings('ignore')
logger = logging.getLogger(__name__)

def filter_data(
    training_file: pd.DataFrame, 
    split: Union[int, str],
    split_info: Dict):
    if split_info["level"] == "agrup":
        splits = np.array_split(training_file["agrup"].unique(), split_info["n_splits"])
        if len(splits) > split:
            return training_file[training_file["agrup"].isin(splits[split])]
    elif split_info["level"] == "group_seg":
        return training_file[training_file["group_seg"] == split]
    return pd.DataFrame()

def join_results(*dfs):
    """
    This node concatenate DataFrames
        Args:
            dfs: List of DataFrames
    """
    results = list(filter(lambda x: x.shape[0] > 0, dfs))
    logger.info(f"# of trained Time Series: {len(results)}")
    if len(results) == 0:
        return pd.DataFrame()
    final_results = pd.concat(results)
    final_results["executed_at"] = datetime.now()
    return final_results