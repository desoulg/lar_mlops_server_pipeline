# Imports
from typing import Optional, Dict, Any, Union, List
import pandas as pd
import numpy as np
from {{cookiecutter.pipeline_name}}.models import *
from ._utils import filter_data
from sklearn.base import BaseEstimator, clone
from tqdm import tqdm
from joblib import Parallel, delayed, cpu_count
import logging
import warnings
warnings.filterwarnings('ignore')

logger = logging.getLogger()



def forecasting(
    training_file: pd.DataFrame,
    split: Union[int, str],
    split_info: Dict,
    best_estimators: pd.DataFrame, 
    fh: int = 12,
    start_date: Optional[str] = None,
    imr_factor: float = 2,
    mr_range: int = 6,
    auto_arima: Optional[Dict[str, Any]] = None) -> pd.DataFrame:
    """
    This node performs forecasting to each serie using best models.
        Args:
            training_fild: DataFrame with series
            best_estimators: Best estimators to each serie
            fh: Size of forecast
        Returns:
            pd.DataFrame -> Forecast Results
    """
    #training_file.reset_index(inplace=True)
    training_file = filter_data(training_file, split, split_info)
    if training_file.shape[0] == 0:
        return pd.DataFrame()
    forecast_file = pd.merge(training_file, best_estimators, how='inner', on='agrup', validate='m:1')
    forecast_file["periodo"] = pd.to_datetime(forecast_file["periodo"]) 
    if start_date is not None:
        forecast_file = forecast_file[forecast_file.periodo.dt.date >= start_date].reset_index(drop=True)
    final_results = pd.DataFrame()
    
    parallel = Parallel(n_jobs=min(cpu_count(), forecast_file.agrup.nunique()), pre_dispatch="2*n_jobs", verbose=2)
    preds = parallel(delayed(fit_and_pred)(g, 
                                           forecast_file[forecast_file.agrup == g], 
                                           auto_arima,fh, imr_factor, mr_range)
                     for g in forecast_file.agrup.unique())
    final_results = pd.concat(preds, ignore_index=True)
    
    final_results["training_end_point"] = training_file.periodo.max()
    test_forecast(final_results, training_file, fh, start_date)
    if auto_arima is None:
        pred_columns = ["forecast", "lower", "upper", "median_pred"]
    else:
        pred_columns = ["forecast", "lower", "upper", "median_pred", "arima_pred"]
    final_results[pred_columns] = final_results[pred_columns].replace([np.inf, -np.inf], np.nan)

    final_results[pred_columns] = final_results[pred_columns].fillna(0).round(0).astype("int32")
    logger.info('# of forecasted Time Series: {}'.format(len(final_results.agrup.unique())))
    
    return final_results



def median_preds(
    ts: pd.Series, 
    estimator: BaseEstimator, 
    fh: int):
    sliding_preds = pd.DataFrame()
    for idx_max in range(fh-1, -1, -1):
        new_ts = ts.iloc[:ts.shape[0]-idx_max]
        new_estimator = clone(estimator)
        try:
            new_estimator.fit(new_ts)
            preds = new_estimator.predict_in_sample(end=len(new_ts)+fh-1)
            preds[preds < 0] = 0
            sliding_preds = pd.concat((sliding_preds, preds))
        except:
            continue

    preds = sliding_preds.groupby(sliding_preds.index).median().yhat
    preds.name = "y_median"
    
    return preds

def base_preds(
    ts: pd.Series, 
    estimator: BaseEstimator, 
    fh: int):
    estimator.fit(ts)
    preds = estimator.predict_in_sample(end=len(ts)+fh-1)
    return preds

def fit_and_pred(
    g: str,
    g_data: pd.DataFrame,
    auto_arima: Optional[Dict[str, Any]],
    fh: int,
    imr_factor: float,
    mr_range: int,
    ):
    
    outfor = pd.DataFrame()
        
    ts = g_data.set_index("periodo").volume_carteira


    if auto_arima is not None:
        estimator = pm.arima.auto_arima(y=ts, **auto_arima["params"])
        arima_pred = pd.Series(estimator.predict_in_sample(end=len(ts)+fh-1, return_conf_int=True)[0], name="arima_pred")
    
    try: 
        estimator = eval(g_data.estimator.iloc[0])
        estimator.fit(ts)
        preds = estimator.predict_in_sample(end=len(ts)+fh-1)
    except:
        estimator = ExponentialSmoothing()
        estimator.fit(ts)
        preds = estimator.predict_in_sample(end=len(ts)+fh-1)
        
    for c in ["yhat", "yhat_lower", "yhat_upper"]:
        if np.isnan(preds[c]).any():
            raise ValueError(estimator, preds[c])
    preds[preds < 0] = 0

    # imr
    median = median_preds(ts,estimator, fh)
    mr = ts[-mr_range:].mean()
    mean = ts.mean()
    upper_line = mean + imr_factor*(mr/1.128)
    bottom_line = mean - imr_factor*(mr/1.128)
    

    median[median > upper_line] = upper_line
    median[median < bottom_line] = bottom_line
    median[median < 0] = 0
    columns = ['level',
                'periodo',
                'agrup',
                'group_seg',
                'volume_carteira', 
                'origin_volume_carteira',
                'custo_unitario']
    outfor = g_data.loc[:, columns].reset_index(drop=True)
    forecast = pd.Series(data=preds.yhat.values, name="forecast")
    upper = pd.Series(data=preds.yhat_upper.values, name="upper")
    lower = pd.Series(data=preds.yhat_lower.values, name="lower")
    median_pred = pd.Series(data=median.values, name="median_pred")
    if auto_arima is None:
        outfor = pd.concat((outfor, forecast, lower, upper, median_pred), axis=1)
    else:
        outfor = pd.concat((outfor, forecast, lower, upper, median_pred, arima_pred), axis=1)

    initial_date = g_data["periodo"].iloc[0]
    adjusted_dates = pd.Series(pd.date_range(start = initial_date, periods = len(ts)+fh, freq='MS'))
    outfor.periodo = adjusted_dates
    # Fill na
    fill_columns = ["agrup", "level", "group_seg", "custo_unitario"]
    outfor.loc[:, fill_columns] = outfor.loc[:, fill_columns].fillna(method="ffill")
    return outfor



def test_forecast(results, training_file, fh=12, start_date = None):
    
    training_file["periodo"] = pd.to_datetime(training_file["periodo"]) 
    if start_date is not None:
        training_file = training_file[training_file.periodo.dt.date >= start_date].reset_index(drop=True)
    results["periodo"] = pd.to_datetime(results.periodo)
    
    max_period = results.groupby("agrup").apply(lambda x: x.periodo.max())
    assert (max_period == max_period.iloc[0]).all(), "Forecasting with different sizes"
    
    assert results[results.volume_carteira.isna()].shape[0] == results.agrup.nunique() * fh, "Out of sample forecasting with not na volume_carteira"
    
    assert (results.groupby(["agrup", "periodo"]).count().forecast <= 1).all(), "Forecast with repeated periods"
    
    assert results.fillna(0).volume_carteira.sum() == training_file.fillna(0).volume_carteira.sum(), "Forecast volume_carteira != Training file volume_carteira"