from typing import Dict

from kedro.pipeline import Pipeline

from {{ cookiecutter.pipeline_name }}.pipelines import preprocess_pipeline as pp
from {{ cookiecutter.pipeline_name }}.pipelines import training_pipeline as ds
from {{ cookiecutter.pipeline_name }}.pipelines import evaluation as ev
from {{ cookiecutter.pipeline_name }}.pipelines import forecast as fr
from {{ cookiecutter.pipeline_name }}.pipelines import big_query as bq
from {{ cookiecutter.pipeline_name }}.pipelines import ingestion_pipeline as ig

import logging 

logger = logging.getLogger(__name__)

def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from a pipeline name to a ``Pipeline`` object.
    """
    big_query_pipeline = bq.create_pipeline()
    ig_pipeline = ig.create_pipeline()
    pp_pipeline = pp.create_pipeline()
    ds_pipeline = ds.create_pipeline()
    ev_pipeline = ev.create_pipeline()
    fr_pipeline = fr.create_pipeline()
    return {
        "bq": big_query_pipeline,
        "ig": ig_pipeline,
        "pp": pp_pipeline,
        "ds": ds_pipeline,
        "ev": ev_pipeline,
        "fr": fr_pipeline,
        "__default__": big_query_pipeline + ig_pipeline + pp_pipeline + ds_pipeline + ev_pipeline + fr_pipeline
    }


