import warnings
warnings.filterwarnings('ignore')

import numpy as np
import pandas as pd
from sklearn.base import RegressorMixin, BaseEstimator
from darts import TimeSeries
from darts.models import RegressionModel as DartsRegressionModel
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.svm import LinearSVR
from sklearn.ensemble import AdaBoostRegressor
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline, make_pipeline

import logging


class BaseRegressionModel(DartsRegressionModel):
    
    def _create_lagged_data(
        self, 
        target_series, 
        past_covariates, 
        future_covariates, 
        max_samples_per_ts
    ):
        n_in = abs(self.lags[0])
        n_out = 1
        data = target_series.values()
        
        n_vars = 1 if type(data) is list else data.shape[1]
        
        df = pd.DataFrame(data)
        cols, names = list(), list()
        # input sequence (t-n, ... t-1)
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
        # forecast sequence (t, t+1, ... t+n)
        for i in range(0, n_out):
            cols.append(df.shift(-i))
            if i == 0:
                names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]

        # put it all together
        agg = pd.concat(cols, axis=1)
        agg.columns = names
        agg.dropna(inplace=True)
        X = agg.iloc[:, :-1].values
        y = agg.iloc[:, -1].values
        return X, y
    
    def _is_probabilistic(self) -> bool:
        return True
    

class RegressionModel(RegressorMixin, BaseEstimator):
    '''

    '''
    def __init__(self, base_estimator, lags=1, poly_degree=1, log_transform=False, **kwargs):
        self._base_estimator = base_estimator
        self.lags = lags
        self.poly_degree = poly_degree
        self.log_transform = log_transform
        self.params = ['lags','poly_degree', 'log_transform'] + list(kwargs.keys())
        for parameter, value in kwargs.items():
            setattr(self, parameter, value)

            
    def fit(self, y, X=None):
        logging.disable(logging.ERROR)
        
        if self.log_transform:
            y = np.log1p(y)
            y = y.apply(np.float64)
        ts = TimeSeries.from_series(y, freq="MS")
        
        self._train_series = ts.copy()
        model_params = self.get_params().copy()
        del model_params["lags"]
        del model_params["poly_degree"]
        del model_params["log_transform"]
        
        steps = []
        if self.poly_degree > 1:
            steps.append(PolynomialFeatures(self.poly_degree))
        steps.append(self._base_estimator(**model_params))
        self._model = BaseRegressionModel(model=make_pipeline(*steps),
                                           lags=self.lags,
                                           lags_past_covariates=None,
                                           lags_future_covariates=None,
                                           )
        self._lagged_data = self._model._create_lagged_data(ts, 
                                                            past_covariates=None, 
                                                            future_covariates=None, 
                                                            max_samples_per_ts=None)
        self._model.fit(ts)
        logging.disable(logging.NOTSET)
        return self
        
    
    def predict(self, n_periods, X=None):
        logging.disable(logging.ERROR)
        pred = self._model.predict(n_periods).values().reshape(-1)
        if self.log_transform:
            pred = np.expm1(pred)
        logging.disable(logging.NOTSET)
        return pred
    
    def predict_in_sample(self, 
                          start=None,
                          end=None,
                          dynamic=False,
                          index=None,
                          method=None,
                          simulate_repetitions=1000):
        logging.disable(logging.ERROR)
        in_sample = self._model.model.predict(self._lagged_data[0])
        y_true = self._lagged_data[1]
        conf = np.nanmean(np.abs(y_true - in_sample))
        
        out_sample = self._model.predict(end - len(self._train_series) + 1).values().reshape(-1)
        #not_predicted = self._train_series.values().reshape(-1)[:self.lags]
        not_predicted = np.empty((self.lags,))
        not_predicted[:] = self._lagged_data[0][0]
        preds = np.concatenate((not_predicted, in_sample, out_sample))
        
        preds = pd.DataFrame({"yhat": preds, "yhat_upper":preds + conf, "yhat_lower": preds - conf})
        preds.index = pd.date_range(start=self._train_series[0].start_time(), 
                                    periods=preds.shape[0], 
                                    freq="MS")
        
        if self.log_transform:
            for col in ["yhat", "yhat_lower", "yhat_upper"]:
                preds[col] = np.expm1(preds[col])
        preds = preds.fillna(0)
        logging.disable(logging.NOTSET)
        
        return preds
    
    def get_params(self, deep=True):
        
        self.parameters = {}
        for p in self.params:
            self.parameters[p] = getattr(self, p)
        return self.parameters
    
    
    
    def set_params(self, **parameters):
        
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
            
        return self

class RandomForestForecaster(RegressionModel):
    def __init__(self, lags=1, **kwargs):
        super(RandomForestForecaster, self).__init__(base_estimator=RandomForestRegressor, lags=lags, **kwargs)

class XGBForecaster(RegressionModel):
    def __init__(self, lags=1, **kwargs):
        kwargs["n_jobs"] = 1
        super(XGBForecaster, self).__init__(base_estimator=XGBRegressor, lags=lags, **kwargs)   
        
class SVRForecaster(RegressionModel):
    def __init__(self, lags=1, **kwargs):
        super(SVRForecaster, self).__init__(base_estimator=LinearSVR, lags=lags, **kwargs)  
        
class AdaForecaster(RegressionModel):
    def __init__(self, lags=1, **kwargs):
        
        super(AdaForecaster, self).__init__(base_estimator=AdaBoostRegressor, lags=lags, **kwargs)  