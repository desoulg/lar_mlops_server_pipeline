# Copyright 2020 QuantumBlack Visual Analytics Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
# NONINFRINGEMENT. IN NO EVENT WILL THE LICENSOR OR OTHER CONTRIBUTORS
# BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF, OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# The QuantumBlack Visual Analytics Limited ("QuantumBlack") name and logo
# (either separately or in combination, "QuantumBlack Trademarks") are
# trademarks of QuantumBlack. The License does not grant you any right or
# license to the QuantumBlack Trademarks. You may not use the QuantumBlack
# Trademarks or any confusingly similar mark as a trademark for your product,
# or use the QuantumBlack Trademarks in any other manner that might cause
# confusion in the marketplace, including but not limited to in advertising,
# on websites, or on software.
#
# See the License for the specific language governing permissions and
# limitations under the License.

"""Project hooks."""
from typing import Any, Dict, Iterable, Optional

from kedro.config import ConfigLoader
from kedro.framework.hooks import hook_impl
from kedro.io import DataCatalog
from kedro.pipeline import Pipeline
from kedro.versioning import Journal
import os

from {{ cookiecutter.pipeline_name }}.utils import CustomSequentialRunner
from {{ cookiecutter.pipeline_name }}.pipelines import big_query as bq
from {{ cookiecutter.pipeline_name }}.pipelines import preprocess_pipeline as pp
from {{ cookiecutter.pipeline_name }}.pipelines import training_pipeline as ds
from {{ cookiecutter.pipeline_name }}.pipelines import evaluation as ev
from {{ cookiecutter.pipeline_name }}.pipelines import forecast as fr
from pathlib import Path


def _change_cwd(older_cwd, new_cwd, conf_dictionary):
    conf_keys_with_filepath = ("filename", "filepath", "path")
    for conf_key, conf_value in conf_dictionary.items():

        # if the conf_value is another dictionary, absolutify its paths first.
        if isinstance(conf_value, dict):
            conf_dictionary[conf_key] = _change_cwd(
                older_cwd, new_cwd, conf_value
            )
            continue

        # if the conf_value is not a dictionary nor a string, skip
        if not isinstance(conf_value, str):
            continue

        # if the conf_value is a string but the conf_key isn't one associated with filepath, skip
        if conf_key not in conf_keys_with_filepath:
            continue
        new_conf_value = conf_value.replace(older_cwd, new_cwd)
        conf_dictionary[conf_key] = new_conf_value
    return conf_dictionary


class ProjectHooks:

    @hook_impl
    def register_config_loader(self, conf_paths: Iterable[str]) -> ConfigLoader:
        self.conf_paths = conf_paths
        self.project_path = conf_paths[0].split("/conf")[0]

        conf_loader = ConfigLoader(self.conf_paths)
        self.config = conf_loader.get('config*')

        self.data_bucket = self.config.get("data_bucket", None)
        project_path = self.data_bucket or self.project_path
        run_id = os.getenv("RUN_ID", "")
        tag = os.getenv("TAG", None)
        tag = tag + "/" if tag is not None else ""

        self.prefix = project_path + tag + run_id

        return conf_loader
    
    @hook_impl
    def before_output_register(self, ds_name):
        prefix = self.prefix + self.config.get("unregistred_data_path", "/data/03_intermediate")
        return prefix + "/" + ds_name + ".parquet"
    
    @hook_impl
    def register_catalog(
        self,
        catalog: Optional[Dict[str, Dict[str, Any]]],
        credentials: Dict[str, Dict[str, Any]],
        load_versions: Dict[str, str],
        save_version: str,
        journal: Journal,
    ) -> DataCatalog:

        if self.data_bucket:
            catalog = _change_cwd(self.project_path, self.prefix, catalog)
        return DataCatalog.from_config(
            catalog, credentials, load_versions, save_version, journal
        )


class ModelingHooks:
    @hook_impl
    def after_pipeline_run(self, 
                            run_params: Dict[str, Any], 
                            pipeline: Pipeline, 
                            catalog: DataCatalog) -> None:
        pass

project_hooks = ProjectHooks()
modeling_hooks = ModelingHooks()