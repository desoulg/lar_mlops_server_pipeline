import kfp
import time
import secrets
import os
import yaml

data = None
tag = os.getenv("TAG", str(secrets.token_urlsafe(4)))

with open("../conf/base/config.yml", "r") as stream:
    try:
        data = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

host = data['kfp_url']

client = kfp.Client(host=host)
pipeline = client.pipeline_uploads.upload_pipeline('../pipeline.yaml', name=data['pipeline_name'] + "_" + tag)

time.sleep(5)
print('** PIPELINE UPLOADED **')
experiments = client.list_experiments().to_dict()