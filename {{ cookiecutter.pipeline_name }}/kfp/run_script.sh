export GIT_PYTHON_REFRESH=quiet

printf "%s" "$SA_WHP" > conf/local/sa_whp.json
printf "whp_gcp_dsl: conf/local/sa_whp.json" > conf/local/credentials.yml

python build_kubeflow_pipeline.py ${IMAGE}

cd kfp/

#python delete_pipeline.py
python upload_pipeline.py