# -*- coding: utf-8 -*-
import re
from pathlib import Path
from typing import Dict, Set

import click

from kfp import dsl, components
from kfp.compiler.compiler import Compiler
import kfp.gcp as gcp

from kedro.framework.session import KedroSession
from kedro.framework.startup import bootstrap_project
from kedro.pipeline.node import Node

from kubernetes.client.models import V1EnvVar
import os

_PIPELINE = None
_IMAGE = None

@click.command()
@click.argument("image", required=True)
@click.option("-p", "--pipeline", "pipeline_name", default=None)
def generate_kfp(image: str, pipeline_name: str, env: str) -> None:
    """Generates a workflow spec yaml file from a Kedro pipeline.

    Args:
        image: container image name.
        pipeline_name: pipeline name to build a workflow spec.
        env: Kedro configuration environment name.

    """
    global _PIPELINE
    global _IMAGE
    _IMAGE = image

    project_path = Path.cwd()
    metadata = bootstrap_project(project_path)

    with KedroSession.create(metadata.package_name, project_path, env=env) as session:
        context = session.load_context()

        pipeline_name = pipeline_name or "__default__"
        _PIPELINE = context.pipelines.get(pipeline_name)

        Compiler().compile(convert_kedro_pipeline_to_kfp, "pipeline.yaml")


@dsl.pipeline(name="{{ cookiecutter.pipeline_name }}", description="Kubeflow pipeline for Kedro project")
def convert_kedro_pipeline_to_kfp() -> None:
    """Convert from a Kedro pipeline into a kfp container graph."""
    node_dependencies = _PIPELINE.node_dependencies
    kfp_ops = _build_kfp_ops(node_dependencies)
    for node, dependencies in node_dependencies.items():
        for dependency in dependencies:
            kfp_ops[node.name].after(kfp_ops[dependency.name])

    metrics_op = _build_metrics_ops()
    metrics_op.after(kfp_ops[node.name])


def _build_metrics_ops():
    tag = os.getenv("TAG", "")
    metrics_op = components.load_component_from_text('''
        name: export_metrics
        outputs:
            - {name: MLPipeline_UI_metadata, type: String}
            - {name: MLPipeline_Metrics, type: String}
        implementation:
            container:
                image: ''' + _IMAGE + '''
                command: ["python"]
                args: ["export_metrics.py"]
                fileOutputs:
                    MLPipeline_UI_metadata: /home/kedro/mlpipeline-ui-metadata.json
                    MLPipeline_Metrics: /home/kedro/mlpipeline-metrics.json 
    ''')()
    {% raw %}
    metrics_op.container.add_env_variable(V1EnvVar(name="RUN_ID", value="{{ workflow.uid }}"))
    {% endraw %}
    metrics_op.container.add_env_variable(V1EnvVar(name="TAG", value=tag))
    return metrics_op


def _build_kfp_ops(
    node_dependencies: Dict[Node, Set[Node]]
) -> Dict[str, dsl.ContainerOp]:
    """Build kfp container graph from Kedro node dependencies. """
    kfp_ops = {}

    tag = os.getenv("TAG", "")

    for node in node_dependencies:
        name = clean_name(node.name)
        cont_op = components.load_component_from_text('''
            name: ''' + name + '''
            implementation:
                container:
                    image: ''' + _IMAGE + '''
                    command: ["kedro", "run", "--node", ''' + node.name + ''',
                              "--runner", "{{ cookiecutter.pipeline_name }}.utils.CustomSequentialRunner"]
        ''')()
        cont_op.execution_options.caching_strategy.max_cache_staleness = "P0D"
        cont_op.set_retry(2)
        cont_op.set_cpu_limit('16')
        cont_op.set_memory_limit('16Gi')
        cont_op.set_cpu_request('8')
        cont_op.set_memory_request('8Gi')
        cont_op.apply(gcp.use_preemptible_nodepool())
        {% raw %}
        cont_op.container.add_env_variable(V1EnvVar(name="RUN_ID", value="{{ workflow.uid }}"))
        {% endraw %}
        cont_op.container.add_env_variable(V1EnvVar(name="TAG", value=tag))
        kfp_ops[node.name] = cont_op
    return kfp_ops


def clean_name(name: str) -> str:
    """Reformat a name.

    Returns:
        name: formatted name.

    """
    return re.sub(r"[\W_]+", "-", name).strip("-")


if __name__ == "__main__":
    generate_kfp()